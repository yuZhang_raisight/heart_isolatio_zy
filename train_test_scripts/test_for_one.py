from torch_net.unet_model import Unet
import os
import os.path
import collections
import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torch_net.trainlogger import TrainLogger
from torch_net import subvol_dataset
from torch_net import unet_model
from torch_net import metrics

def train(spacing, gamma, subvol_size, norm=True, rotate=False, lr=0.001, loss_strategy='BCE',
          num_channels=32, data_len=1, batch_size=1, num_epochs=200, window=(-1024, 1500), scale=False,
          translate=False, out_channels=1, activate='sigmoid', dilated=0, enhance=0, cordia=0):
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    device = torch.device('cuda')

    config = '_bs' + str(batch_size) \
            + '_gm' + str(gamma) \
            + '_lr' + str(lr) \
            + '_loss' + loss_strategy \
            + '_sz' + str(subvol_size[0]) \
            + '_' + str(subvol_size[1]) \
            + '_' + str(subvol_size[2]) \
            + '_spc' + str(spacing) \
            + '_norm' + str(norm) \
            + '_rotate' + str(rotate) \
            + '_window' + str(window[0]) \
            + '_' + str(window[1]) \
            + '_scale' + str(scale) \
            + '_translate' + str(translate) \
            + '_out' + str(out_channels) \
            + '_acti' + activate \
            + '_dila' + str(dilated) \
            + '_enhance' + str(enhance) \
            + '_cordia' + str(cordia) \
            + '_ch' + str(num_channels)

    dataroot = '/media/data/heart_isolation/hdf5_1'
    trainroot = '/media/data/heart_isolation/train_1/train' + '_datalen' + str(data_len) + config

    if not os.path.exists(trainroot):
        os.makedirs(trainroot)

    if not os.path.exists(trainroot):
        os.mkdir(trainroot)
    logfile = os.path.join(trainroot, 'log.txt')
    auxiliary = False

    net = unet_model.Unet(channels=num_channels, out_channels=out_channels, activate=activate, auxiliary=auxiliary)

    if torch.cuda.is_available():
        net = net.to(device)

    optimizer = optim.Adam(net.parameters(), lr=lr, weight_decay=1e-4)
    logger = TrainLogger(logfile)
    scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=gamma)
    for epoch in range(num_epochs):

        dataset = subvol_dataset.SubvolDataset(dataroot=dataroot, subvol_size=subvol_size, spacing=spacing,
                                               datalen=data_len, norm=norm, window=window, num_labels=out_channels,
                                               dilated=dilated, enhance=enhance, cordia=cordia)

        loader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=batch_size)

        for idx, data in enumerate(loader):

            x = torch.tensor(data['image'], device=device, dtype=torch.float)
            y = torch.tensor(data['label'], device=device, dtype=torch.float)

            y_pred = net(x)
            if enhance != 0:
                z = torch.tensor(data['mask'], device=device, dtype=torch.float)
                if loss_strategy == 'wBCE':
                    ratio = y.sum() / y.numel()
                    z += y * (1 - ratio) + (1 - y) * ratio
                    loss = F.binary_cross_entropy(y_pred[0], y, weight=z)
                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])
                elif loss_strategy == 'BCE':
                    z += 1
                    loss = F.binary_cross_entropy(y_pred[0], y, weight=z)
                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])
                elif loss_strategy == 'bf1':
                    z += 1
                    loss = F.binary_cross_entropy(y_pred[0], y, weight=z) + metrics.bf1_loss(y_pred[0], y)
                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])
                elif loss_strategy == 'ddice':
                    z1 = torch.tensor(data['mask1'], device=device, dtype=torch.float)
                    z += 1
                    loss0 = F.binary_cross_entropy(y_pred[0], y, weight=z)
                    loss1 = metrics.distance_dice(y, y_pred[0], z1)
                    loss = loss0 + loss1

                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])
            else:
                if loss_strategy == 'dice':
                    if out_channels == 3:
                        loss = metrics.dice_multi_loss(y, y_pred[0], activate)
                        acc = metrics.accuracy_multi(y, y_pred[0], activate)
                        dice = metrics.dice_multi(y, y_pred[0], activate)
                    else:
                        loss = metrics.dice_loss(y, y_pred[0])
                        acc = metrics.accuracy_heart(y, y_pred[0])
                        dice = metrics.dice(y, y_pred[0])
                elif 'wdice' in loss_strategy:
                    # e.g. wdice1_0.5_1
                    w = loss_strategy.split('wdice')[-1]
                    w = w.split('_')
                    w = [float(s) for s in w]
                    loss = metrics.dice_weight(y, y_pred[0], w)
                    acc = metrics.accuracy_multi(y, y_pred[0], activate)
                    dice = metrics.dice_multi(y, y_pred[0], activate)
                elif loss_strategy == 'BCE':
                    loss = F.binary_cross_entropy(y_pred[0], y)
                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])
                elif loss_strategy == 'ddice':
                    z = torch.tensor(data['mask'], device=device, dtype=torch.float)
                    loss = metrics.distance_dice(y, y_pred[0], z)
                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])

                elif loss_strategy == 'dice+BCE':
                    loss = F.binary_cross_entropy(y_pred[0], y) + metrics.dice_loss(y, y_pred[0])

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            scheduler.step()

            # log
            log = collections.OrderedDict()
            log['loss'] = float(loss)
            log['acc'] = float(acc)
            log['dice'] = float(dice)
            logger.log(epoch, idx, log)

        # save model
        savepath = os.path.join(trainroot, 'epoch' + str(epoch) + '.pth')
        torch.save(net.state_dict(), savepath)

    print('done subvol_size:%s gamma:%s\t spacing:%s\n' % (subvol_size, gamma, spacing))


if __name__ == '__main__':
    train(spacing=0.2, gamma=0.98, subvol_size=(128, 128, 80), out_channels=1, activate='sigmoid',
          enhance=0, cordia=0, num_epochs=200, batch_size=1, loss_strategy='BCE')

