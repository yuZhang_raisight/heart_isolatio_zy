import SimpleITK as sitk
import torch
import numpy as np
import os
import glob

from collections import OrderedDict
from torch_net import unet_model, resunet_model, denseunet_model, unet_model_plus
from milib.imageutils import resample_image, resize_3darray, get_min_value_sitkimage
from milib.seg_testbed import extract_seg_from_prob, measure_seg
from multiprocessing.pool import Pool

def mkdir(pth):
    if not os.path.exists(pth):
        os.mkdir(pth)

def get_model(modelpath, channels, out=1, acti='softmax'):
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    device = torch.device('cuda')
    state_dict = torch.load(modelpath)
    if 'resunet' in modelpath:
        net = resunet_model.ResUnet(channels, out, acti, False)
    elif 'denseunet' in modelpath:
        net = denseunet_model.DenseUnet(channels, out, acti, False)
    elif 'unet+' in modelpath:
        net = unet_model_plus.Unet_Plus(channels, out, acti, False)

    else:
    # state_dict = fix_parallel_model(state_dict)
        net = unet_model.Unet(channels=channels, out_channels=out, activate=acti, auxiliary=False)

    net.load_state_dict(state_dict)
    net = net.to(device)
    net.eval()
    return net

def fix_parallel_model(state_dict):
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = k[7:]
        new_state_dict[name] = v
    return new_state_dict

def parse_model_name(model_path):
    pieces = model_path.split('/')
    epoch_name = pieces[-1]
    epoch_name = epoch_name.replace('epoch', '')
    epoch_name = epoch_name.replace('.pth', '')
    epoch = int(epoch_name)

    model_name = pieces[-2]
    pieces_size = model_name.split('_sz')
    pieces_size = pieces_size[1].split('_spc')
    size = pieces_size[0].split('_')
    size = [int(x) for x in size]
    size = tuple(size)

    pieces = pieces[-2].split('_spc')[1]
    spacing = float(pieces.split('_')[0])
    channels = int(pieces.split('_ch')[1])
    pieces = pieces.split('_norm')[-1]
    norm = pieces.split('_rotate')[0]
    norm = True if norm == 'True' else False

    pieces = pieces.split('_window')[-1]
    windows = pieces.split('_scale')[0]
    windows = [int(x) for x in windows.split('_')]

    pieces = pieces.split('_scale')[-1]
    scale = pieces.split('_translate')[0]
    scale = True if scale == 'True' else False

    pieces = pieces.split('_translate')[-1]
    translate = pieces.split('_out')[0]
    translate = True if translate == 'True' else False

    pieces = pieces.split('_out')[-1]
    out = pieces.split('_acti')[0]
    out = int(out)

    pieces = pieces.split('_acti')[-1]
    acti = pieces.split('_dila')[0]

    pieces = pieces.split('_dila')[-1]
    dila = int(pieces.split('_enhance')[0])
    return size, spacing, channels, epoch, model_name, norm, windows, scale, translate, out, acti, dila

def predict(inputfile, net, sub_size=(176, 176, 64), spacing=0.15, norm=False, window=(0, 0)):
    if isinstance(spacing, float) or isinstance(spacing, int):
        spacing = (spacing, spacing, spacing)

    testimg0 = sitk.ReadImage(inputfile)
    if window[0] != window[1]:
        default_value = window[0]
    else:
        default_value = get_min_value_sitkimage(testimg0)

    testimg = resample_image(testimg0, spacing, size_out=sub_size, default_value=default_value)

    if window[0] != window[1]:
        testimg = sitk.IntensityWindowing(testimg, window[0], window[1], window[0], window[1])
    if norm:
        testimg = sitk.Cast(testimg, sitk.sitkFloat32)
        testimg = sitk.RescaleIntensity(testimg, 0, 1)
    img = sitk.GetArrayFromImage(testimg)
    device = torch.device('cuda')

    with torch.no_grad():
        sub_size_np = list(sub_size)
        sub_size_np.reverse()
        inputsub = np.zeros([1, 1] + sub_size_np)
        inputsub[0, 0, :, :, :] = img
        x = torch.tensor(inputsub, device=device, dtype=torch.float, requires_grad=False)
        y = net(x)[0]
        pred = y.data.cpu().numpy()
        result = resize_3darray(pred, sub_size_np)

    # save results
    if result.ndim == 3:
        resultimg = sitk.GetImageFromArray(result)
        resultimg.SetDirection(testimg0.GetDirection())
        resultimg.SetOrigin(testimg0.GetOrigin())
        resultimg.SetSpacing(spacing)
        resultimg_back = resample_image(resultimg, testimg0.GetSpacing(), size_out=testimg0.GetSize(), default_value=0)
        resultimg_back.SetOrigin(testimg0.GetOrigin())
    else:
        resultimgs = []
        for i in range(3):
            resultimg = sitk.GetImageFromArray(result[:,:,:,i])
            resultimg.SetDirection(testimg0.GetDirection())
            resultimg.SetOrigin(testimg0.GetOrigin())
            resultimg.SetSpacing(spacing)
            resultimg = resample_image(resultimg, testimg0.GetSpacing(), size_out=testimg0.GetSize(),
                                            default_value=i//2)
            resultimgs.append(sitk.GetArrayFromImage(resultimg))
        resultimg_back = np.stack(resultimgs, axis=3)
        resultimg_back = sitk.GetImageFromArray(resultimg_back)
        resultimg_back.CopyInformation(testimg0)
    return resultimg_back

def get_segmentation(inputfile, modelpath, result_dir=None, threshold=0.5):
    name = os.path.basename(inputfile)
    # get coronary result
    imgsize, spacing, channels, epoch, model_name, norm, window, scale, translate, out, acti, dila\
        = parse_model_name(modelpath)
    net = get_model(modelpath, channels, out, acti)
    prob = predict(inputfile, net, sub_size=imgsize, spacing=spacing, norm=norm, window=window)
    seg = extract_seg_from_prob(prob, thresh=0.5, morph=True)

    del net
    savedir = os.path.join(result_dir, model_name + '_ep' + str(epoch))
    if result_dir:
        if not os.path.exists(savedir):
            os.makedirs(savedir)

        savename = name.replace('_image.nii', '_prob.nii.gz')
        savepath = os.path.join(savedir, savename)
        sitk.WriteImage(prob, savepath)

        savename = name.replace('_image.nii', '_seg' + str(threshold) + '.nii.gz')
        savepath = os.path.join(savedir, savename)
        sitk.WriteImage(seg, savepath)

    return prob

def test_imagelist(test_data_dir, result_dir, modelpath, gamma=0.98, threshold=0.5):
    # test_data_dir = r'/home/huangxs/data/Kidney_part1/test_nii'
    # result_dir = r'/home/huangxs/data/Kidney_part1/test_abdominal'
    # modelpath = '/home/huangxs/data/Kidney_part1/train_abdominal/cases16_bs3_gm0.98_center0.8_sz256_256_64_spc0.5_ch32/epoch79.pth'

    # modelpath = '/home/huangxs/data/Kidney_part1/train_abdominal/' + cases + '_bs' + str(batchszie) + '_gm' + str(gamma) + '_center' + str(center) \
    #            + '_sz' + str(subvol_size[0]) + '_' + str(subvol_size[1]) + '_' + str(subvol_size[2]) + '_spc' + str(spacing) +'_ch32/epoch79.pth'

    # 判断savedir文件夹是否存在
    imgsize, spacing, channels, epoch, model_name, norm, window, scale, translate, out, acti, dila\
        = parse_model_name(modelpath)
    savedir = os.path.join(result_dir, model_name + '_ep' + str(epoch))
    if (not os.path.exists(savedir)) and os.path.exists(modelpath):

        for dr in (test_data_dir, result_dir):
            if not os.path.exists(dr):
                os.makedirs(dr)

        # imgfiles = glob.glob(os.path.join(test_data_dir, '*_image.nii'))
        imgfiles = glob.glob(os.path.join(test_data_dir,'*_image.nii'))  # '/home/huangxs/Raysightmed/Kidney_part1/nii/CAO_LONGXIANG/CAO_LONGXIANG_image.nii'
        imgfiles1 = glob.glob(os.path.join(test_data_dir,'*.nii.gz'))
        imgfiles = imgfiles + imgfiles1
        imgfiles = sorted(imgfiles)
        for imgfile in imgfiles:
            get_segmentation(imgfile, modelpath, result_dir)

        print('Done gamma:%s\t spacing:%s subvol_size:%s\n' % (gamma, spacing, imgsize))

    elif os.path.exists(savedir) and os.path.exists(modelpath):
        # imgfiles = glob.glob(os.path.join(test_data_dir, '*_image.nii'))
        imgfiles = glob.glob(os.path.join(test_data_dir, '*/*_image.nii'))  # '/home/huangxs/Raysightmed/Kidney_part1/nii/CAO_LONGXIANG/CAO_LONGXIANG_image.nii'
        imgfiles = sorted(imgfiles)
        for imgfile in imgfiles:
            result_case_file = os.path.join(result_dir, model_name + '_ep' + str(epoch), os.path.basename(imgfile).replace('_image.nii', '_seg' + str(threshold) + '.nii.gz')) # case segmentation result file "LIN_XIAOHUI_seg0.5_8neighbor.nii.gz"
            if not os.path.exists(result_case_file):
                get_segmentation(imgfile, modelpath, result_dir)
                print('Down case segmentation:', str(result_case_file))
            else:
                print('Exist case segmentation:', str(result_case_file))
        print('Exist testing result gamma:%s\t spacing:%s subvol_size:%s \t in:%s \n' % (gamma, spacing, imgsize, savedir))
    elif (not os.path.exists(savedir) ) and (not os.path.exists(modelpath)):
        print('No training result gamma:%s\t spacing:%s subvol_size:%s\t in:%s \n ' % (gamma, spacing, imgsize, modelpath))
    else:
        print('Delete training result in:%s' % (modelpath))

def statistic_segmentation_result(seg_folder_path, cases, label_folder_path):

    def process_statistic(file_name, savepath):
        image = sitk.ReadImage(file_name)
        name = os.path.basename(file_name)
        if '_seg0.5' in name:
            label_name = name.replace('_seg0.5', '_label')

        label_file_name = os.path.join(label_folder_path, label_name)
        label_image = sitk.ReadImage(label_file_name)

        dice, sensi, spec, voxel_fp = measure_seg(label_image, image)

        with open(savepath, 'a') as f:
            f.write(name + '\n')
            f.write(
                '\t' + 'sensitivity:' + str(sensi) + '\t' + 'specificity:' + str(spec) + '\t' + 'dice_rate:' + str(dice) + '\t' + 'false positive_rate:' + str(voxel_fp) + '\n\n')

        # print(file_name)

    #seg_folder_path = r'/home/huangxs/data/Kidney_part1/test_abdominal'
    subfolder_path = glob.glob(os.path.join(seg_folder_path, cases, '*_seg0.5*'))

    for folder_num in range(subfolder_path.__len__()):
        savepath = subfolder_path[folder_num].replace('.nii.gz', '_testing_statistic.csv')

        if os.path.exists(savepath):
            # print('Exist & delete this file:%s' % (savepath))
            # os.remove(savepath)

            # print('Exist this file:%s' % (savepath))
            continue
        process_statistic(subfolder_path[folder_num], savepath)

def find_best_epoch(logfile, indicator='loss'):
    with open(logfile, 'r') as f:
        lines = f.readlines()
        init_epoch = 0
        losses = []
        all_loss = []
        for line in lines:
            it = line.split(' ')
            epoch, batch, loss, acc, dice = int(it[1]), int(it[3]), float(it[5]), float(it[7]), float(it[9])
            if indicator == 'loss':
                eva = loss
            elif indicator == 'dice':
                eva = dice
            if epoch == init_epoch:
                losses.append(eva)
            else:
                all_loss.append(np.mean(losses))
                losses = []
                losses.append(eva)
                init_epoch += 1
        if indicator == 'loss':
            best_epoch = np.argmin(all_loss)
        elif indicator == 'dice':
            best_epoch = np.argmax(all_loss)
        return best_epoch

def test_epoch(model_folder, result_dir, indicator, epoch=None):
    logfile = model_folder + '/log.txt'
    test_data_dir = r'/media/data/heart_isolation/nii_test'
    if epoch is None:
        epoch = find_best_epoch(logfile, indicator=indicator)

    cases = os.path.basename(model_folder) + '_ep' + str(epoch)
    modelpath = model_folder + '/epoch' + str(epoch) + '.pth'

    test_imagelist(test_data_dir, result_dir, modelpath)
    statistic_segmentation_result(result_dir, cases, test_data_dir)

def batch_test(modelspth = '/media/data/heart_isolation/train',
               result_dir = '/media/data/heart_isolation/test', multi=False):
    models = [os.path.join(modelspth, f) for f in sorted(os.listdir(modelspth)) if 'train_' in f]
    if multi:
        from multiprocessing import Pool
        paras = []
        for i in models:
            paras.append((i, 'loss'))
        with Pool(2) as p:
            p.starmap(test_epoch, result_dir, paras)
    else:
        for i in models:
            test_epoch(i, result_dir, 'loss')

def results_statis(results_pth):
    results = [os.path.join(results_pth, f) for f in sorted(os.listdir(results_pth)) if '.csv' in f]
    results_stat = np.zeros((4, len(results)))

    for n, i in enumerate(results):
        with open(i, 'r') as f:
            lines = f.readlines()[1]
            stat = lines.split('\t')
            sensitivity, specificity, dice, false_positive = float(stat[1].split(':')[-1]), \
                                                             float(stat[2].split(':')[-1]), \
                                                             float(stat[3].split(':')[-1]), \
                                                             float(stat[4].split(':')[-1])

            results_stat[0, n] = sensitivity
            results_stat[1, n] = specificity
            results_stat[2, n] = dice
            results_stat[3, n] = false_positive
    mea = np.mean(results_stat, axis=1)
    std = np.std(results_stat, axis=1)
    print(os.path.basename(results_pth))
    print('dice' + '\t'+'sensitivity' + '\t' + 'sepcificity' + '\t' + 'false_ppositive')
    print('%.4f+-%.4f\t%.4f+-%.4f\t%.4f+-%.4f\t%.4f+-%.4f'%(mea[2], std[2], mea[0], std[0],mea[1], std[1],
                                                            mea[3], std[3]))

def batch_results_statis(results_pth='/media/data/heart_isolation/test'):
    results = [os.path.join(results_pth, f) for f in sorted(os.listdir(results_pth))]
    for r in results:
        results_statis(r)
    # results = [(f, ) for f in results]
    # with Pool(len(results)) as p:
    #     p.starmap(results_statis, results)

def apply_mask(imgsdir, segsdir, outsdir):
    imgs = [os.path.join(imgsdir, f) for f in sorted(os.listdir(imgsdir)) if '_image.nii' in f]
    for i in imgs:
        fn = os.path.basename(i)
        sub = fn.split('_image.nii')[0]
        mask_pth = os.path.join(segsdir, os.path.join(segsdir, sub+'_seg0.5.nii.gz'))
        if not os.path.exists(mask_pth):
            continue
        img = sitk.ReadImage(i)
        mask = sitk.ReadImage(mask_pth)
        maskarr = sitk.GetArrayFromImage(mask)
        imgarr = sitk.GetArrayFromImage(img)
        imgarr[maskarr == 0] = -1024
        out = sitk.GetImageFromArray(imgarr)
        out.CopyInformation(img)
        sitk.WriteImage(out, os.path.join(outsdir, sub + '_mask.nii.gz'))

def batch_apply_mask(resultsdir = '/media/data/heart_isolation/test_concat'):
    imgsdir = '/media/data/heart_isolation/nii_test'
    outsdir = resultsdir + '_mask'
    mkdir(outsdir)
    segs = [os.path.join(resultsdir, f) for f in sorted(os.listdir(resultsdir))]
    outdirs = []
    for i in segs:
        outdir = os.path.join(outsdir, os.path.basename(i))
        if os.path.exists(outdir):
            print('Existing ' + os.path.basename(i))
        else:
            mkdir(outdir)
        outdirs.append(outdir)
    paras = [(imgsdir, i, j) for i, j in zip(segs, outdirs)]
    with Pool(len(segs)) as p:
        p.starmap(apply_mask, paras)


def clear_models(model_folder):
    logfile = model_folder + '/log.txt'
    epoch = find_best_epoch(logfile, indicator='loss')
    epochs = sorted(glob.glob(os.path.join(model_folder, '*.pth')))
    for e in epochs:
        fn = os.path.basename(e)
        n = 'epoch' + str(epoch) + '.pth'
        if n != fn:
            os.remove(e)

def batch_clear_models(model_floders):
    models = [os.path.join(model_floders, f) for f in sorted(os.listdir(model_floders))]
    for r in models:
        clear_models(r)

def coronary_include_rate(pred_dir):
    preds = [os.path.join(pred_dir, f) for f in sorted(os.listdir(pred_dir)) if 'seg0.5.nii.gz' in f]
    cor_dir = '/media/data/heart_isolation/manual_labels/cornoary_vessels'
    rates = []
    for p in preds:
        fn = os.path.basename(p)
        fn = fn.split('_seg0.5')[0]
        cor_pth = os.path.join(cor_dir, fn + '_coronary_label.nii.gz')
        pred = sitk.ReadImage(p)
        cor = sitk.ReadImage(cor_pth)
        pred = sitk.GetArrayFromImage(pred)
        cor = sitk.GetArrayFromImage(cor)
        rate = np.sum(pred * cor) / np.sum(cor)
        rates.append(rate)
    conditions = os.path.basename(pred_dir)
    print(conditions)
    conditions = conditions.split('_enhance')[-1]
    enhance = conditions.split('_cordia')[0]
    conditions = conditions.split('_cordia')[-1]
    dilate = conditions.split('_ch')[0]
    print(enhance + dilate + ':' + '%.4f' % np.mean(rates) + '+-' + '%.4f' % np.std(rates))
    # print(str(np.mean(rates)) + '+-' + str(np.std(rates)))

def batch_cor_rate(con_pth):
    con_dirs = [os.path.join(con_pth, f) for f in sorted(os.listdir(con_pth))]
    for i in con_dirs:
        coronary_include_rate(i)

def bone_remove(model_folder, test_data_dir, result_dir, epoch=None):
    logfile = model_folder + '/log.txt'
    if not epoch:
        epoch = find_best_epoch(logfile, indicator='loss')
    modelpath = model_folder + '/epoch' + str(epoch) + '.pth'
    test_imagelist(test_data_dir, result_dir, modelpath)


def component_analysis(seg_pth, mask_pth, out_pth, threshold=0.8):
    if not os.path.exists(out_pth):
        seg = sitk.ReadImage(seg_pth)
        mask = sitk.ReadImage(mask_pth)
        mask_arr = sitk.GetArrayFromImage(mask)
        seg_Connected = sitk.ConnectedComponent(seg, True)
        seg_Connected_np = sitk.GetArrayFromImage(seg_Connected)
        connect_num = np.max(seg_Connected_np)
        out_arr = np.zeros_like(mask_arr)
        for i in range(1, connect_num + 1):
            component = seg_Connected_np == i
            rate = np.sum(component * mask_arr) / np.sum(component)
            if rate > threshold:
                out_arr += component

        out_arr[out_arr > 0] = 1
        out_arr = out_arr.astype(np.uint8)
        out = sitk.GetImageFromArray(out_arr)
        out.CopyInformation(seg)
        sitk.WriteImage(out, out_pth)
    else:
        print('exist:' + os.path.basename(out_pth))

def component_analysis_v2(seg_pth, mask_pth, out_pth, threshold1=0.8, threshold2=0.4):
    if not os.path.exists(out_pth):
        seg = sitk.ReadImage(seg_pth)
        mask = sitk.ReadImage(mask_pth)
        mask_arr = sitk.GetArrayFromImage(mask)
        seg_Connected = sitk.ConnectedComponent(seg, True)
        seg_Connected_np = sitk.GetArrayFromImage(seg_Connected)
        connect_num = np.max(seg_Connected_np)
        out_arr = np.zeros_like(mask_arr)
        for i in range(1, connect_num + 1):
            component = seg_Connected_np == i
            rate = np.sum(component * mask_arr) / np.sum(component)
            if rate > threshold2:
                if rate > threshold1:
                    out_arr += component
                else:
                    zs = np.where(component == 1)[0]
                    z_centroid = np.mean(zs)
                    if z_centroid > seg.GetSize()[2] / 3:
                        out_arr += component

        out_arr[out_arr > 0] = 1
        out_arr = out_arr.astype(np.uint8)
        out = sitk.GetImageFromArray(out_arr)
        out.CopyInformation(seg)
        sitk.WriteImage(out, out_pth)
    else:
        print('exist:' + os.path.basename(out_pth))

def batch_component_analysis(seg_dir, mask_dir, out_dir, version=component_analysis):
    mkdir(out_dir)
    from multiprocessing import Pool
    paras = []
    seg_list = [os.path.join(seg_dir, f) for f in sorted(os.listdir(seg_dir)) if '_seg0.5.nii.gz' in f]
    for s in seg_list:
        if 'precent' in s:
            fn = os.path.basename(s)
            subfn = fn.split('_45precent_seg0.5.nii.gz')[0]
            paras.append((s, os.path.join(mask_dir, subfn + '_seg0.5.nii.gz'),
                          os.path.join(out_dir, subfn + '_45precent_component.nii.gz')))
        else:
            fn = os.path.basename(s)
            subfn = fn.split('_seg0.5.nii.gz')[0]
            paras.append((s, os.path.join(mask_dir, subfn + '_seg0.5.nii.gz'),
                          os.path.join(out_dir, subfn + '_component.nii.gz')))
    if len(seg_list) < 12:
        pool_num = len(seg_list)
    else:
        pool_num = 12
    with Pool(pool_num) as p:
        p.starmap(version, paras)

def component_results_analysis(result_dir='/media/data/cases_collect_for_Jun/component/mask_v2/U-Net_PlusPlus_concat_v1_0.8',
                               xls_dir='/media/data/cases_collect_for_Jun/component_v1.xlsx'):
    import openpyxl
    lab_dir = '/media/data/cases_collect_for_Jun/Labels_new'
    lab_list = [os.path.join(lab_dir, f) for f in sorted(os.listdir(lab_dir))]
    title = os.path.basename(result_dir)
    if os.path.exists(xls_dir):
        workbook = openpyxl.load_workbook(xls_dir)
        workbook.create_sheet(title)
        sheet = workbook[title]
    else:
        workbook = openpyxl.Workbook()
        sheet = workbook.active
        sheet.title = os.path.basename(result_dir)
    sheet.cell(1, 1, 'ID')
    sheet.cell(1, 2, 'rate')
    sheet.cell(1, 3, 'dice')
    sheet.cell(1, 4, 'sensi')
    sheet.cell(1, 5, 'spec')
    sheet.cell(1, 6, 'voxel_fp')
    for n, i in enumerate(lab_list):
        fn = os.path.basename(i)
        subfn = fn.split('_coronary_label_tree_vein')[0]
        lab = sitk.ReadImage(i)
        result = sitk.ReadImage(os.path.join(result_dir, subfn + '_component.nii.gz'))
        sheet.cell(n+2, 1, subfn)
        if lab.GetSize() != result.GetSize():
            # worksheet.write(n, 1, 'wrong size')
            continue
        dice, sensi, spec, voxel_fp = measure_seg(lab, result)
        pred = sitk.GetArrayFromImage(lab)
        cor = sitk.GetArrayFromImage(result)
        rate = np.sum(pred * cor) / np.sum(pred)
        sheet.cell(n+2, 2, rate)
        sheet.cell(n+2, 3, dice)
        sheet.cell(n+2, 4, sensi)
        sheet.cell(n+2, 5, spec)
        sheet.cell(n+2, 6, voxel_fp)
    workbook.save(xls_dir)

if __name__ == '__main__':
    # train_ = 'train_model'
    # test_ = 'test_model'
    # test_one_model = True
    # if test_one_model:
    #     model_path = '/media/data/heart_isolation/train_model/train_datalen280_bs1_gm0.98_lr0.001_lossBCE_sz128_128_80_spc0.2_normTrue_rotateTrue_window-1024_1500_scaleTrue_translateTrue_out1_actisigmoid_dila0_enhance3_cordia5_modelunet+_ch32'
    #     result_dir = '/media/data/heart_isolation/test_model'
    #     test_epoch(model_path, result_dir, 'loss', 39)
    # else:
    #     batch_test(modelspth='/media/data/heart_isolation/' + train_,
    #                result_dir='/media/data/heart_isolation/' + test_)
    # batch_results_statis(results_pth='/media/data/heart_isolation/' + test_)
    # batch_apply_mask(resultsdir='/media/data/heart_isolation/' + test_)
    # batch_cor_rate('/media/data/heart_isolation/' + test_)






    model_folder = '/media/yuzhang/Elements/heart_isolation/train_enhance/train_datalen28_bs3_gm0.98_lr0' \
                   '.001_lossBCE_sz128_128_80_spc0.2_normTrue_rotateFalse_window-1024_1500_scaleFalse_translateFalse_out1_actisigmoid_dila0_enhance3_cordia5_ch32'
    test_data_dir = '/media/yuzhang/Elements/heart_isolation/xingsheng'

    bone_remove(model_folder, test_data_dir, test_data_dir)

    # model_folder = '/media/data/heart_isolation/train_2/train_datalen280_bs3_gm0.98_lr0.001_lossBCE_sz128_128_80_spc0.2_normTrue_rotateFalse_window-1024_1500_scaleFalse_translateFalse_out1_actisigmoid_dila0_enhance0_cordia0_ch32'
    # test_data_dir = r'/media/data/heart_isolation/nii_test'
    # result_dir = '/media/data/heart_isolation/test_2'
    # epoch = 99
    # cases = os.path.basename(model_folder) + '_ep' + str(epoch)
    # modelpath = model_folder + '/epoch' + str(epoch) + '.pth'
    #
    # test_imagelist(test_data_dir, result_dir, modelpath)
    # statistic_segmentation_result(result_dir, cases, test_data_dir)

    # segsdir = '/media/data/cases_collect_for_Jun/mask'
    # apply_mask(test_data_dir, segsdir, '/media/data/cases_collect_for_Jun/mask_apply/U-Net_PlusPlus_concat')

    # mask_dir = '/media/data/cases_collect_for_Jun/mask_v2'
    # seg_dir = '/media/data/cases_collect_for_Jun/segmentation_result/U-Net_Plus'
    # out_dir = '/media/data/cases_collect_for_Jun/component/mask_v2/U-Net_Plus_v1_0.8'
    # batch_component_analysis(seg_dir, mask_dir, out_dir, component_analysis)
    #
    # seg_dir = '/media/data/cases_collect_for_Jun/segmentation_result/U-Net_PlusPlus_concat'
    # out_dir = '/media/data/cases_collect_for_Jun/component/mask_v2/U-Net_PlusPlus_concat_v1_0.8'
    # batch_component_analysis(seg_dir, mask_dir, out_dir, component_analysis)
    #
    # seg_dir = '/media/data/cases_collect_for_Jun/segmentation_result/U-Net_Plus'
    # out_dir = '/media/data/cases_collect_for_Jun/component/mask_v2/U-Net_Plus_v2_0.8_0.4'
    # batch_component_analysis(seg_dir, mask_dir, out_dir, component_analysis_v2)
    #
    # seg_dir = '/media/data/cases_collect_for_Jun/segmentation_result/U-Net_PlusPlus_concat'
    # out_dir = '/media/data/cases_collect_for_Jun/component/mask_v2/U-Net_PlusPlus_concat_v2_0.8_0.4'
    # batch_component_analysis(seg_dir, mask_dir, out_dir, component_analysis_v2)

    # component_results_analysis(result_dir='/media/data/cases_collect_for_Jun/component/mask_v2/U-Net_Plus_v1_0.8',
    #                            xls_dir='/media/data/cases_collect_for_Jun/component_v1.xlsx')
    # component_results_analysis(
    #     result_dir='/media/data/cases_collect_for_Jun/component/mask_v2/U-Net_PlusPlus_concat_v1_0.8',
    #     xls_dir='/media/data/cases_collect_for_Jun/component_v1.xlsx')
    #
    # component_results_analysis(
    #     result_dir='/media/data/cases_collect_for_Jun/component/mask_v2/U-Net_Plus_v2_0.8_0.4',
    #     xls_dir='/media/data/cases_collect_for_Jun/component_v2.xlsx')
    # component_results_analysis(
    #     result_dir='/media/data/cases_collect_for_Jun/component/mask_v2/U-Net_PlusPlus_concat_v2_0.8_0.4',
    #     xls_dir='/media/data/cases_collect_for_Jun/component_v2.xlsx')

