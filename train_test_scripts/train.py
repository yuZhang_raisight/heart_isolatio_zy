import os
import os.path
import collections
import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torch_net.trainlogger import TrainLogger
from torch_net import subvol_dataset
from torch_net import unet_model, resunet_model, denseunet_model, unet_model_plus
from torch_net import metrics

import shutil

def save_tensor(tensr, path):
    import SimpleITK as sitk
    import numpy as np
    nparr = np.squeeze(tensr.cpu().data.numpy()[0, 0, :, :, :])
    img = sitk.GetImageFromArray(nparr)
    sitk.WriteImage(img, path)


def train(spacing, gamma, subvol_size, norm=True, noise=False, std=0, rotate=False, lr=0.001,
          loss_strategy='BCE', num_channels=32, data_len=28, batch_size=3, num_epochs=200,
          window=(-1024, 1500), scale=False, translate=False, out_channels=1, activate='sigmoid',
          dilated=0, enhance=0, cordia=3, model='unet'):
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    device = torch.device('cuda')
    # spacing = 1.5   #1.0
    # gamma = 0.98
    # center = 0.8

    config = '_bs' + str(batch_size) \
            + '_gm' + str(gamma) \
            + '_lr' + str(lr) \
            + '_loss' + loss_strategy \
            + '_sz' + str(subvol_size[0]) \
            + '_' + str(subvol_size[1]) \
            + '_' + str(subvol_size[2]) \
            + '_spc' + str(spacing) \
            + '_norm' + str(norm) \
            + '_rotate' + str(rotate) \
            + '_window' + str(window[0]) \
            + '_' + str(window[1]) \
            + '_scale' + str(scale) \
            + '_translate' + str(translate) \
            + '_out' + str(out_channels) \
            + '_acti' + activate \
            + '_dila' + str(dilated) \
            + '_enhance' + str(enhance) \
            + '_cordia' + str(cordia) \
            + '_model' + str(model) \
            + '_ch' + str(num_channels)

    dataroot = '/media/data/heart_isolation/hdf5_v3'
    trainroot = '/media/data/heart_isolation/train_model/train' + '_datalen' + str(data_len) + config
    if enhance != 0:
        trainroot = '/media/data/heart_isolation/train_model/train' + '_datalen' + str(data_len) + config
    if noise:
        trainroot = '/media/data/heart_isolation/train/noiseTrue' + str(std) \
                    + '_datalen' + str(data_len) + config

    if not os.path.exists(trainroot):
        os.makedirs(trainroot)

    logfile = os.path.join(trainroot, 'log.txt')
    auxiliary = False

    if model == 'unet':
        net = unet_model.Unet(channels=num_channels, out_channels=out_channels, activate=activate,
                              auxiliary=auxiliary)
    elif model == 'resunet':
        net = resunet_model.ResUnet(num_channels, 1, 'sigmoid', False)
    elif model == 'denseunet':
        net = denseunet_model.DenseUnet(num_channels, 1, 'sigmoid', False)
    elif model == 'unet+':
        net = unet_model_plus.Unet_Plus(num_channels, 1, 'sigmoid', False)
    elif model == 'unet++':
        net = unet_model_plus.Unet_Plus_Plus_concatenate(num_channels, 1, 'sigmoid', False)

    model_list = [os.path.join(trainroot, f) for f in os.listdir(trainroot) if '.pth']
    if len(model_list) != 0:
        net.load_state_dict(torch.load(model_list[-1]))
    if torch.cuda.is_available():
        net = net.to(device)

    if torch.cuda.device_count() > 1:
        print("Let's use", torch.cuda.device_count(), "GPUs!")
        net = nn.DataParallel(net)

    optimizer = optim.Adam(net.parameters(), lr=lr, weight_decay=1e-4)
    logger = TrainLogger(logfile)
    scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=gamma)
    for epoch in range(num_epochs):


        dataset = subvol_dataset.SubvolDataset(
            dataroot=dataroot, subvol_size=subvol_size, spacing=spacing, datalen=data_len,
            norm=norm, window=window, num_labels=out_channels, dilated=dilated, enhance=enhance,
            cordia=cordia)

        loader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=batch_size)

        for idx, data in enumerate(loader):

            x = torch.tensor(data['image'], device=device, dtype=torch.float)
            y = torch.tensor(data['label'], device=device, dtype=torch.float)

            y_pred = net(x)
            if enhance != 0:
                z = torch.tensor(data['mask'], device=device, dtype=torch.float)
                if loss_strategy == 'wBCE':
                    ratio = y.sum() / y.numel()
                    z += y * (1 - ratio) + (1 - y) * ratio
                    loss = F.binary_cross_entropy(y_pred[0], y, weight=z)
                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])
                elif loss_strategy == 'BCE':
                    z += 1
                    loss = F.binary_cross_entropy(y_pred[0], y, weight=z)
                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])
                elif loss_strategy == 'bf1':
                    z += 1
                    loss = F.binary_cross_entropy(y_pred[0], y, weight=z) + metrics.bf1_loss(y_pred[0], y)
                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])
                elif loss_strategy == 'ddice':
                    z1 = torch.tensor(data['mask1'], device=device, dtype=torch.float)
                    z += 1
                    loss0 = F.binary_cross_entropy(y_pred[0], y, weight=z)
                    loss1 = metrics.distance_dice(y, y_pred[0], z1)
                    loss = loss0 + loss1

                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])
            else:
                if loss_strategy == 'dice':
                    if out_channels == 3:
                        loss = metrics.dice_multi_loss(y, y_pred[0], activate)
                        acc = metrics.accuracy_multi(y, y_pred[0], activate)
                        dice = metrics.dice_multi(y, y_pred[0], activate)
                    else:
                        loss = metrics.dice_loss(y, y_pred[0])
                        acc = metrics.accuracy_heart(y, y_pred[0])
                        dice = metrics.dice(y, y_pred[0])
                elif 'wdice' in loss_strategy:
                    # e.g. wdice1_0.5_1
                    w = loss_strategy.split('wdice')[-1]
                    w = w.split('_')
                    w = [float(s) for s in w]
                    loss = metrics.dice_weight(y, y_pred[0], w)
                    acc = metrics.accuracy_multi(y, y_pred[0], activate)
                    dice = metrics.dice_multi(y, y_pred[0], activate)
                elif loss_strategy == 'BCE':
                    loss = F.binary_cross_entropy(y_pred[0], y)
                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])
                elif loss_strategy == 'ddice':
                    z = torch.tensor(data['mask'], device=device, dtype=torch.float)
                    loss = metrics.distance_dice(y, y_pred[0], z)
                    acc = metrics.accuracy_heart(y, y_pred[0])
                    dice = metrics.dice(y, y_pred[0])

                elif loss_strategy == 'dice+BCE':
                    loss = F.binary_cross_entropy(y_pred[0], y) + metrics.dice_loss(y, y_pred[0])

                if auxiliary:
                    loss += metrics.mask_bceloss(y, y_pred[1]) * 0.2 + metrics.mask_bceloss(y, y_pred[2]) * 0.2

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            scheduler.step()

            # log
            log = collections.OrderedDict()
            log['loss'] = float(loss)
            log['acc'] = float(acc)
            log['dice'] = float(dice)
            logger.log(epoch, idx, log)

        # save model
        savepath = os.path.join(trainroot, 'epoch' + str(epoch) + '.pth')
        torch.save(net.state_dict(), savepath)

    print('done subvol_size:%s gamma:%s\t spacing:%s\n' % (subvol_size, gamma, spacing))


if __name__ == '__main__':
    # train(spacing=0.2, gamma=0.98, subvol_size=(128, 128, 80), out_channels=1, activate='sigmoid',
    #       enhance=3, cordia=5, num_epochs=100, batch_size=1, loss_strategy='BCE', data_len=280,
    #       model='denseunet')
    train(spacing=0.2, gamma=0.98, subvol_size=(128, 128, 80), out_channels=1, activate='sigmoid',
          enhance=3, cordia=5, num_epochs=100, batch_size=1, loss_strategy='BCE', data_len=280,
          model='unet++', rotate=False, translate=False, scale=False)








