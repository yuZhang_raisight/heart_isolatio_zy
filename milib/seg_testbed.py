import SimpleITK as sitk
import numpy as np
import milib.imageutils as imageutils
from milib.imageutils import _morphlogical

def prec_img(img):
    if isinstance(img, str):
        result = sitk.ReadImage(img)
    elif isinstance(img, sitk.Image):
        result = img
    else:
        raise TypeError('img should be a file path of sitk.Image')
    return result


def sensitivity(ref, seg):
    """
    calculate the sensitivity and specificity of segmentation against reference
    :param ref: reference label file or sitk image
    :param seg: segmentation file or sitk image
    :return: (sensitivity, specificity)
    """

    refimg = prec_img(ref)
    segimg = prec_img(seg)

    ref_sum = sumimg(refimg)
    ref_cl_covered = sitk.And(refimg > 0, segimg > 0)
    covered_sum = sumimg(ref_cl_covered)
    sensi = covered_sum / ref_sum
    return sensi


def sumimg(img):
    return np.sum(sitk.GetArrayFromImage(img))

def voxel_overlap(ref, seg):
    """
    Overlap of two segmentations: union(A, B)/(A + B)
    :param ref: reference file or sitk image
    :param seg: segmentation file or sitk image
    :return: overlap of the voxels
    """
    refimg = prec_img(ref)
    segimg = prec_img(seg)
    union = sitk.And(refimg > 0, segimg > 0)

    return sumimg(union)/(0.5 * (sumimg(refimg) + sumimg(segimg)))

def voxel_falsepositive(ref, seg):
    """
    False positive in term of voxels
    :param ref: reference file or sitk image
    :param seg: segmentation file or sitk image
    :return: false positive in term of voxels
    """
    refimg = prec_img(ref)
    segimg = prec_img(seg)
    union = sitk.And(refimg > 0, segimg > 0)

    return 1.0 - sumimg(union)/sumimg(segimg)


def extract_seg_from_prob(prob, thresh=0.5, morph=True):
    if prob.GetNumberOfComponentsPerPixel() == 1:
        orig_seg = sitk.BinaryThreshold(prob, thresh, 1.0)
    else:
        prob_arr = sitk.GetArrayFromImage(prob)
        prob_arr = np.argmax(prob_arr, axis=3)
        prob_arr = ~prob_arr.astype(np.bool)
        orig_seg = sitk.GetImageFromArray(prob_arr.astype(np.uint8))
        orig_seg.CopyInformation(prob)
    if morph:
        orig_seg = _morphlogical(orig_seg, radius=3, simple=True)
    return orig_seg


def measure_seg(label, seg):
    dice = voxel_overlap(label, seg)
    voxel_fp = voxel_falsepositive(label, seg)
    sensi = sensitivity(label, seg)
    spec = sensitivity(seg, label)
    return dice, sensi, spec, voxel_fp


def measure_segresult_dir(refdir, segdir, save_cleanseg=True, force_connect=True):
    import glob
    import os
    ref_files = sorted(glob.glob(os.path.join(refdir, '*_coronary_label.nii.gz')))
    results = []
    result_file = os.path.join(segdir, 'results.csv')
    marks = [',', ',', ',', '\n']

    with open(result_file, 'w') as f:
        f.write('case name, sensitivity, specificity, voxel false positives\n')

    for ref_file in ref_files:
        basename = os.path.basename(ref_file)
        probname = basename.replace('_coronary_label.nii.gz', '_coronary_prob.nii.gz')
        prob_file = os.path.join(segdir, probname)
        if not os.path.exists(prob_file):
            print('not found: ', prob_file)
            continue
        ref = sitk.ReadImage(ref_file)
        prob = sitk.ReadImage(prob_file)
        if force_connect:
            seg = extract_seg_from_prob(ref, prob, thresh=0.5)
        else:
            seg = prob > 0.5

        if save_cleanseg:
            segname = prob_file.replace('_prob', '_cleanseg')
            sitk.WriteImage(seg, segname)
        sensi, spec, voxel_fp = measure_seg(ref, seg)
        result = [basename.replace('_coronary_label.nii.gz', ''), sensi, spec, voxel_fp]
        results.append(result)

        with open(result_file, 'a') as f:
            for i in range(4):
                f.write(str(result[i]) + marks[i])

    scores = [result[1:] for result in results]
    scores = np.array(scores)
    average_scores = np.mean(scores, axis=0)
    result = ['average', average_scores[0], average_scores[1], average_scores[2]]

    with open(result_file, 'a') as f:
        for i in range(4):
            f.write(str(result[i]) + marks[i])


def test_testbed():
    label_file = '/work/data/unittests_data/seg_testbed/03_CSJ_20170802_coronary_label.nii.gz'
    prob_file = '/work/data/unittests_data/seg_testbed/03_CSJ_20170802_coronary_prob.nii.gz'
    label = sitk.ReadImage(label_file)
    prob = sitk.ReadImage(prob_file)

    two_components = extract_seg_from_prob(label, prob)
    # two_components = imageutils.find_n_largest_components(connect_seg, 2)
    sitk.WriteImage(two_components,
                    '/work/data/unittests_data/seg_testbed/03_CSJ_20170802_coronary_connect_seg0.5.nii.gz')

    se, sp, vo = measure_seg(label, two_components)
    print('sensitivity', se,  'specifity', sp, 'voxel false positive', vo)


def test_measure_segresult_dir():
    ref_dir = '/work/data/data_subvol/RecentCases/testing_nii'
    seg_dir = '/work/data/data_subvol/segresult/multigpu_pos5.0_batch1_eval0.8_sz224_224_224_spc0.07_ch24_ep159'
    measure_segresult_dir(ref_dir, seg_dir)


def test_measure_segresult_dirs():
    from multiprocessing import Pool

    ref_dir = '/work/data/data_subvol/RecentCases/testing_nii'
    seg_dirs = [
        '/work/data/data_subvol/segresult/multigpu_pos5.0_batch1_eval0.8_sz224_224_224_spc0.07_ch24_ep159',
        '/work/data/data_subvol/segresult/multigpu_pos5.0_batch1_eval0.8_sz224_224_224_spc0.07_ch24_ep159/fullseg',
        '/work/data/data_subvol/segresult/recent_pos4.0_batch8_eval0.8_sz128_128_128_spc0.06_ch32_ep159'
        ]

    paras = [(ref_dir, seg_dir, False, False)
             for seg_dir in seg_dirs]

    with Pool(3) as p:
        p.starmap(measure_segresult_dir, paras)


if __name__ == '__main__':
    # test_testbed()
    # test_measure_segresult_dir()
    test_measure_segresult_dirs()