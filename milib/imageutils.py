import SimpleITK as sitk
import numpy as np
import math
from skimage.measure import label, regionprops

def resample_image(sitk_img, spacing_out, resample_method=sitk.sitkLinear, size_out=None, default_value=None):
    """
    resample an sitk image with certain output spacing
    :param sitk_img: input sitk image
    :param spacing_out: a 3-element list or tuple
    :param resample_method: sitk.sitkLinear or sitk.sitkNearestNeighbor
    :param size_out: output size in (nx, ny, nz)
    :return: resampled image
    """
    dx_out, dy_out, dz_out = spacing_out
    nx, ny, nz = sitk_img.GetSize()
    dx_in, dy_in, dz_in = sitk_img.GetSpacing()
    nx_out, ny_out, nz_out = int(math.ceil(nx*dx_in/dx_out)), int(math.ceil(ny*dy_in/dy_out)), int(math.ceil(nz*dz_in/dz_out))
    resampler = sitk.ResampleImageFilter()
    resampler.SetInterpolator(resample_method)
    resampler.SetOutputSpacing(spacing_out)
    resampler.SetOutputDirection(sitk_img.GetDirection())
    if default_value is not None:
        resampler.SetDefaultPixelValue(default_value)

    if size_out is None:
        size_out = [int(nx_out), int(ny_out), int(nz_out)]
        origin = sitk_img.GetOrigin()
    else:
        origin = reset_origin_sitkimage(sitk_img, spacing_out, size_out)

    resampler.SetSize(size_out)
    resampler.SetOutputOrigin(origin)
    result = resampler.Execute(sitk_img)
    return result

def _morphlogical(sitk_img, radius=3, simple=True):
    """
    a template function for morphlogical operation like dilation, erosion, open, close etc.
    :param sitk_img:
    :param radius:
    :return:
    """
    if not simple:
        sitk_img = sitk.BinaryDilate(sitk_img, radius)
    seg_arr = sitk.GetArrayFromImage(sitk_img)
    labels = label(seg_arr)
    props = regionprops(labels)
    max_area = 0
    max_index = 0
    for i in range(len(props)):
        if props[i].area > max_area:
            max_area = props[i].area
            max_index = i
    bbox = props[max_index].bbox
    seg_arr = np.zeros_like(seg_arr)
    seg_arr[bbox[0]:bbox[3], bbox[1]:bbox[4], bbox[2]:bbox[5]] = props[max_index].image
    output = sitk.GetImageFromArray(seg_arr)
    output.CopyInformation(sitk_img)
    output = sitk.BinaryFillhole(output)
    if not simple:
        output = sitk.BinaryErode(output, radius)
    return output

def binary_dilate(sitk_img, radius=1, foreground=1):
    """
    Dilate an binary image with radius
    """
    return _morphlogical(sitk_img, 'binary_dilate', radius=radius, foreground=foreground)


def _start_end_1d(size_in, centr1d, size_out):
    start_offset = int(centr1d - size_out//2)
    start_pair = (0, -start_offset) if start_offset < 0 else (start_offset, 0)
    end_offset = int(centr1d + (size_out+1)//2 - size_in)
    end_pair = (size_in, size_out-end_offset) if end_offset > 0 else (centr1d + (size_out+1)//2, size_out)
    return start_pair, end_pair


def crop_3darray(input_array, centr, size3d_out, padvalue=0):
    """
    :param arr: the input 3d array
    :param centr: a 3-tuple, center of the crop
    :param size3d: a 3-tuple, range of crop
    :param padvalue: padded value of out of bound
    :return: a cropped array
    """
    result = np.ones(size3d_out) * padvalue
    size3d_in = input_array.shape

    for i in range(3):
        if centr[i] - size3d_out[i]//2 >= size3d_in[i] or centr[i] + (size3d_out[i]+1)//2 < 0:
            return result

    x_start, x_end = _start_end_1d(size3d_in[0], centr[0], size3d_out[0])
    y_start, y_end = _start_end_1d(size3d_in[1], centr[1], size3d_out[1])
    z_start, z_end = _start_end_1d(size3d_in[2], centr[2], size3d_out[2])
    result[x_start[1]:x_end[1], y_start[1]:y_end[1], z_start[1]:z_end[1]] \
        = input_array[x_start[0]:x_end[0], y_start[0]:y_end[0], z_start[0]:z_end[0]]

    return result


def fill_3darray(source_array, target_array, centr):
    """
    Fill a target array with source array
    :param source_array: the source array
    :param target_array: array to be filled. After the operation, this array is changed.
    :param centr: the position of center of source array in the target array
    :return:
    """
    size3d_source = source_array.shape
    size3d_target = target_array.shape

    for i in range(3):
        if centr[i] - size3d_source[i]//2 >= size3d_target[i] or centr[i] + (size3d_source[i]+1)//2 < 0:
            return

    x_start, x_end = _start_end_1d(size3d_target[0], centr[0], size3d_source[0])
    y_start, y_end = _start_end_1d(size3d_target[1], centr[1], size3d_source[1])
    z_start, z_end = _start_end_1d(size3d_target[2], centr[2], size3d_source[2])

    target_array[x_start[0]:x_end[0], y_start[0]:y_end[0], z_start[0]:z_end[0]] = \
        source_array[x_start[1]:x_end[1], y_start[1]:y_end[1], z_start[1]:z_end[1]]


def crop_3dtensor(inputs, cropping, data_format='channels_first'):
    """
    crop a 5-D tensor
    :param inputs: the input 5-d tensor
    :param cropping: the crop of one size in x, y, z
    :param data_format: "channels_last" or "channels_first"
    :return: the cropped tensor
    """
    crop0 = cropping[0]
    crop1 = cropping[1]
    crop2 = cropping[2]

    if data_format == 'channels_last':
        sz = inputs.shape[1:4]
        return inputs[:, crop0:sz[0]-crop0, crop1:sz[1]-crop1, crop2:sz[2]-crop2, :]
    else:
        sz = inputs.shape[2:5]
        return inputs[:, :, crop0:sz[0]-crop0, crop1:sz[1]-crop1, crop2:sz[2]-crop2]


def get_image_from_array(arr, imgtemplate=None):
    """
    Get the sitk image from numpy array using a image template
    :param arr: input numpy array
    :param imgtemplate: sitk image template
    :return: sitk image result
    """
    resultimg = sitk.GetImageFromArray(arr)
    if imgtemplate is not None:
        resultimg.SetDirection(imgtemplate.GetDirection())
        resultimg.SetOrigin(imgtemplate.GetOrigin())
        resultimg.SetSpacing(imgtemplate.GetSpacing())

    return resultimg


def read_dicoms(dicomdir):
    reader = sitk.ImageSeriesReader()
    dicom_names = reader.GetGDCMSeriesFileNames(dicomdir)
    reader.SetFileNames(dicom_names)
    image = reader.Execute()
    return image

def reset_origin_sitkimage(sitk_image, new_spacing, new_size):
    new_size = np.array(new_size, dtype=np.uint64)
    size = np.array(sitk_image.GetSize(), dtype=np.uint64)
    origin = np.array(sitk_image.GetOrigin(), dtype=np.float64)
    spacing = np.array(sitk_image.GetSpacing(), dtype=np.float64)
    direction = np.array(sitk_image.GetDirection(), dtype=np.float64)
    direction = np.reshape(direction, (3, 3))
    center = origin + np.matmul(direction, 0.5 * size * spacing)
    new_origin = center - np.matmul(direction, 0.5 * new_size * new_spacing)
    return new_origin

def resize_3darray(pred_arr, target_size):

    size = np.array(pred_arr.shape[2:], dtype=np.uint16)
    target_size = np.array(target_size, dtype=np.uint16)

    for i, j in zip(size, target_size):
        if i % 2 == 1 or j % 2 == 1:
            raise Exception('Wrong! Odd size!')

    if pred_arr.shape[1] == 1:
        out = pred_arr[0, 0, : ,: ,:]
    else:
        out = pred_arr[0, :, :, :, :]

    if np.where(size - target_size < 0)[0].shape[0] > 0:
        dx, dy, dz = (target_size - size) // 2
        if pred_arr.shape[1] == 1:
            out = np.pad(out, ((dx, dx), (dy, dy), (dz, dz)), constant_values=0)
        else:
            outs = []
            outs.append(np.pad(out[0], ((dx, dx), (dy, dy), (dz, dz)), constant_values=0))
            outs.append(np.pad(out[1], ((dx, dx), (dy, dy), (dz, dz)), constant_values=0))
            outs.append(np.pad(out[2], ((dx, dx), (dy, dy), (dz, dz)), constant_values=1))
            out = np.stack(outs, axis=3)
    else:
        dx, dy, dz = (size - target_size) // 2
        if pred_arr.shape[1] == 1:
            out = out[dx:size[0]-dx, dy:size[1]-dy, dz:size[2]-dz]
        else:
            out = out[:, dx:size[0] - dx, dy:size[1] - dy, dz:size[2] - dz]
            out = out.transpose((1,2,3,0))

    return out

def get_min_value_sitkimage(sitk_image):
    if isinstance(sitk_image, str):
        img = sitk.ReadImage(sitk_image)
    else:
        img = sitk_image
    arr = sitk.GetArrayFromImage(img)
    return int(arr.min())

def get_center_sitkimage(sitk_image):
    size = np.array(sitk_image.GetSize(), dtype=np.uint64)
    origin = np.array(sitk_image.GetOrigin(), dtype=np.float64)
    spacing = np.array(sitk_image.GetSpacing(), dtype=np.float64)
    direction = np.array(sitk_image.GetDirection(), dtype=np.float64)
    direction = np.reshape(direction, (3, 3))

    center = origin + np.matmul(direction, 0.5 * size * spacing)
    return center

def get_augmented_sitkimage(sitk_image, scale, translate, interpolator, default_value):
    augmenter = sitk.AffineTransform(3)
    augmenter.SetCenter(get_center_sitkimage(sitk_image))
    augmenter.Scale(scale)
    augmenter.Translate(translate)

    out = sitk.Resample(sitk_image, sitk_image.GetSize(), augmenter, interpolator, sitk_image.GetOrigin(),
                        sitk_image.GetSpacing(), sitk_image.GetDirection(), default_value)
    return out