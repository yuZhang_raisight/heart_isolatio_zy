import SimpleITK as sitk
import h5py as h5
import numpy as np


def write_h5image(img, h5file, chunks=True):
    """
    :param img: input image file, assumed to be nii, mha etc. It could also be an sitk Image object
    :param h5file: h5 output file
    :param chunks:
    :return: None
    """
    if isinstance(img, str):
        img = sitk.ReadImage(img)
    arr = sitk.GetArrayFromImage(img)
    
    with h5.File(h5file, 'w') as f:
        f.create_dataset('image', data=arr, dtype=arr.dtype, chunks=chunks, compression='gzip')
        size = np.array(img.GetSize(), dtype=np.uint64)
        origin = np.array(img.GetOrigin(), dtype=np.float64)
        spacing = np.array(img.GetSpacing(), dtype=np.float64)
        direction = np.array(img.GetDirection(), dtype=np.float64)
        f.create_dataset('size', data=size)
        f.create_dataset('origin', data=origin)
        f.create_dataset('spacing', data=spacing)
        f.create_dataset('direction', data=direction)


def crop_h5image(f, mincorner, size):
    """
    Crop image with origin and size
    :param f: h5 File that contains the image
    :param mincorner: the corner of min index in the order of [x, y, z]
    :param size: the size of the output in the order of [x, y, z]
    :return: sitk image
    """
    # notice that the order of the dimension is not the same
    arr3d = f['image'][mincorner[2]:mincorner[2] + size[2],
                       mincorner[1]:mincorner[1] + size[1],
                       mincorner[0]:mincorner[0] + size[0]]

    direction = f['direction']
    direction = np.reshape(direction, (3, 3))
    origin0 = f['origin']
    spacing = f['spacing']
    mincorner = np.array(mincorner)
    origin = origin0 + np.matmul(direction, mincorner * spacing)
    img = sitk.GetImageFromArray(arr3d)
    img.SetOrigin(tuple(origin))
    img.SetDirection(tuple(direction.flat))
    img.SetSpacing(tuple(spacing))
    return img


def angles_to_matrix(angles):
    """
    :param angles: a 3-d tuple
    :return: a 3 by 3 rotation matrix
    """
    a, b, c = angles
    R1 = np.array([1, 0, 0, 0, np.cos(a), -np.sin(a), 0, np.sin(a), np.cos(a)])
    R1 = np.reshape(R1, (3, 3))
    R2 = np.array([np.cos(b), 0, np.sin(b), 0, 1, 0, -np.sin(b), 0, np.cos(b)])
    R2 = np.reshape(R2, (3, 3))
    R3 = np.array([np.cos(c), -np.sin(c), 0, np.sin(c), np.cos(c), 0, 0, 0, 1])
    R3 = np.reshape(R3, (3, 3))
    R = np.matmul(R1, R2)
    R = np.matmul(R, R3)
    return R


def resample_sitkimage(sitk_image, center, size, spacing, direction=None,
                       default_value=0, interp='linear', dtype=sitk.sitkFloat32):
    """
    :param sitk_image:
    :param center: numpy array 3d vector or tuple
    :param size: numpy array 3d int vector or tuple
    :param spacing: numpy array 3d vector or tuple
    :param direction: numpy array 3d vector
    :param default_value: for extrapolation if out of bound
    :param interp: interpolation method, 'linear' or 'nearest'
    :param dtype: pixel data type
    :return: sitk image
    """
    if direction is None:
        direction = np.reshape(np.array(sitk_image.GetDirection()), (3, 3))

    if not isinstance(direction, np.ndarray):
        direction = np.reshape(np.array(direction), (3, 3))

    center = np.array(center)
    size = np.array(size, dtype=np.uint64)
    spacing = np.array(spacing)

    origin = center - np.matmul(direction, 0.5 * size * spacing)
    resampler = sitk.ResampleImageFilter()
    resampler.SetSize(tuple(int(s) for s in size))
    resampler.SetOutputOrigin(tuple(origin))
    resampler.SetOutputSpacing(tuple(spacing))
    resampler.SetOutputDirection(tuple(direction.flat))
    resampler.SetDefaultPixelValue(default_value)
    resampler.SetOutputPixelType(dtype)
    # if interp == 'nearest':
    #     interpolator = sitk.sitkNearestNeighbor
    # else:
    #     interpolator = sitk.sitkLinear

    if interp == 'linear':   #add 20190717
        interpolator = sitk.sitkLinear
    if interp == 'gaussion':
        interpolator = sitk.sitkGaussian
    if interp == 'labelgaussion':
        interpolator = sitk.sitkLabelGaussian
    if interp == 'BSpline':
        interpolator = sitk.sitkBSpline
    if interp == 'lanczos':
        interpolator = sitk.sitkLanczosWindowedSinc
    if interp == 'nearest':
        interpolator = sitk.sitkNearestNeighbor

    resampler.SetInterpolator(interpolator)
    output = resampler.Execute(sitk_image)
    return output


unit_corners = [
    np.array([-0.5, -0.5, -0.5]),
    np.array([-0.5, -0.5, 0.5]),
    np.array([-0.5, 0.5, -0.5]),
    np.array([-0.5, 0.5, 0.5]),
    np.array([0.5, -0.5, -0.5]),
    np.array([0.5, -0.5, 0.5]),
    np.array([0.5, 0.5, -0.5]),
    np.array([0.5, 0.5, 0.5])
]


def resample_h5image(h5filename, center, size, spacing, rot_angles=(0, 0, 0),
                     default_value=0, interp='linear', dtype=sitk.sitkFloat32):
    """
    Resample a subvolume from a h5 file
    :param h5filename:
    :param center: physical center. (3,) numpy array or tuple
    :param size: size of the output image.  (3,) numpy int array or tuple
    :param spacing: spacing of the output image.  (3,) numpy array or tuple
    :param rot_angles: rotation angles. (3,) numpy array or tuple
    :param default_value: default extrapolation value if out of bound. For CT HU, should use -1000.0
    :param interp: interpolation method. 'linear' or 'nearest'
    :param dtype: pixel datatype.
    :return: a sitk Image
    """
    def makeinbound(coord, size):
        coord = np.maximum(coord, [0, 0, 0])
        coord = np.minimum(coord, size-1)
        coord = coord.astype(np.int64)
        return coord

    def get_index_corners(f, center, size, spacing, direction):
        origin0 = np.array(f['origin'][:])
        spacing0 = np.array(f['spacing'][:])
        size0 = np.array(f['size'][:])
        direction0 = np.reshape(np.array(f['direction'][:]), (3, 3))

        index_corners = []
        for unit_corner in unit_corners:
            physical_corner = center + np.matmul(direction, unit_corner * size * spacing)
            index_corner = (np.matmul(physical_corner - origin0, direction0))/spacing0
            index_corners.append(index_corner)

        index_corners = np.array(index_corners)
        min_corner = np.floor(np.min(index_corners, axis=0)).astype(np.int64)
        min_corner = makeinbound(min_corner, size0)
        max_corner = np.ceil(np.max(index_corners, axis=0)).astype(np.int64)
        max_corner = makeinbound(max_corner, size0)
        outsize = max_corner - min_corner + 1
        return min_corner, max_corner, outsize

    with h5.File(h5filename, 'r') as f:
        direction0 = np.reshape(np.array(f['direction'][:]), (3, 3))
        rot_matrix = angles_to_matrix(rot_angles)
        direction = np.matmul(rot_matrix, direction0)
        min_corner, max_corner, cropsize = get_index_corners(f, center, size, spacing, direction)
        img = crop_h5image(f, min_corner, cropsize)
        resampled_img = resample_sitkimage(img, center=center, size=size, spacing=spacing,
                                           direction=direction, default_value=default_value, interp=interp, dtype=dtype)
        return resampled_img

def get_center_h5image(h5filename):
    with h5.File(h5filename, 'r') as f:
        direction = f['direction'][:]
        direction = np.reshape(direction, (3, 3))
        origin = f['origin'][:]
        spacing = f['spacing'][:]
        size = f['size'][:]
        center = origin + np.matmul(direction, 0.5 * size * spacing)
        return center

def get_min_value_h5image(h5filename):
    with h5.File(h5filename, 'r') as f:
        imgarr = f['image'][:]
        return int(imgarr.min())