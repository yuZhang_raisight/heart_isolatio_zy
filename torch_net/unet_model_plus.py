import torch.nn as nn
import torch.nn.functional as F
import torch_net.unet_components as unet_components
import torch_net.unet_plus_component as unet_plus_comp
import torch
from milib.imageutils import crop_3dtensor




class Unet_Plus(nn.Module):

    def __init__(self, channels, out_channels=1, acticvate='sigmoid', auxiliary=False):
        super().__init__()
        self.channels = channels
        self.inconv1 = unet_components.InConv(channels)
        self.down2 = unet_components.Down(channels, channels * 2, layers=1)
        self.down3 = unet_components.Down(channels * 2, channels * 4, layers=3)
        self.down4 = unet_components.Down(channels * 4, channels * 8, layers=2)
        self.down5 = unet_components.Down(channels * 8, channels * 8, layers=1)

        # up4 is at the same level as down4
        self.up4 = unet_components.Up(channels * 8, channels * 8, channels * 4)
        # up3 is at the same level as down3
        self.up3 = unet_components.Up(channels * 4, channels * 4, channels * 2)
        self.up3_mid = unet_plus_comp.Up_Plus(channels * 4, channels * 8, channels * 4)
        # up2 is at the same level as up1
        self.up2 = unet_components.Up(channels * 2, channels * 2, channels)
        self.up2_mid = unet_plus_comp.Up_Plus(channels * 2, channels * 4, channels * 2)
        # up1 is at the same level as conv1
        self.up1 = unet_components.Up(channels, channels, channels, layers=1)
        self.up1_mid = unet_plus_comp.Up_Plus(channels, channels * 2, channels)
        self.outconv1 = unet_components.OutConv(channels, out_channels, acticvate, cropping=0)
        self.auxiliary = auxiliary
        if auxiliary:
            self.outconv2 = unet_components.OutConv(channels, cropping=16)
            self.outconv3 = unet_components.OutConv(channels * 2, cropping=16)


    def forward(self, x):
        x0_0 = self.inconv1(x)

        x1_0 = self.down2(x0_0)
        x0_1 = self.up1_mid(x0_0, x1_0)

        x2_0 = self.down3(x1_0)
        x1_1 = self.up2_mid(x1_0, x2_0)
        x0_2 = self.up1_mid(x0_1, x1_1)

        x3_0 = self.down4(x2_0)
        x2_1 = self.up3_mid(x2_0, x3_0)
        x1_2 = self.up2_mid(x1_1, x2_1)
        x0_3 = self.up1_mid(x0_2, x1_2)

        x4_0 = self.down5(x3_0)
        x3_1 = self.up4(x3_0, x4_0)
        x2_2 = self.up3(x2_1, x3_1)
        x1_3 = self.up2(x1_2, x2_2)
        x0_4 = self.up1(x0_3, x1_3)

        output1 = self.outconv1(x0_4)

        # for auxiliary classifier
        if self.auxiliary:
            output2 = F.upsample(x1_3, scale_factor=2)
            output2 = self.outconv2(output2)
            output3 = F.upsample(x2_2, scale_factor=4)
            output3 = self.outconv3(output3)
            output = [output1, output2, output3]
        else:
            output = [output1]

        return output



class Unet_Plus_Plus_concatenate(nn.Module):

    def __init__(self, channels, out_channels=1, acticvate='sigmoid', auxiliary=False):
        super().__init__()
        self.channels = channels
        self.inconv1 = unet_components.InConv(channels)
        self.down2 = unet_components.Down(channels, channels * 2, layers=1)
        self.down3 = unet_components.Down(channels * 2, channels * 4, layers=3)
        self.down4 = unet_components.Down(channels * 4, channels * 8, layers=2)
        self.down5 = unet_components.Down(channels * 8, channels * 8, layers=1)

        concate = 3
        # up4 is at the same level as down4
        self.up4 = unet_components.Up(channels * 8, channels * 8, channels * 4)
        # up3 is at the same level as down3
        self.up3 = unet_plus_comp.Up_Plus(channels * 4 * 2, channels * 4, channels * 2)
        self.up3_1_mid = unet_plus_comp.Up_Plus(channels * 4, channels * 8, channels * 4)
        # up2 is at the same level as up1
        self.up2 = unet_plus_comp.Up_Plus(channels * 2 * 3, channels * 2, channels)
        self.up2_1_mid = unet_plus_comp.Up_Plus(channels * 2, channels * 4, channels * 2)
        self.up2_2_mid = unet_plus_comp.Up_Plus(channels * 2 * 2, channels * 4, channels * 2)
        # up1 is at the same level as conv1
        self.up1 = unet_plus_comp.Up_Plus(channels * 4, channels, channels, layers=1)
        self.up1_1_mid = unet_plus_comp.Up_Plus(channels, channels * 2, channels)
        self.up1_2_mid = unet_plus_comp.Up_Plus(channels * 2, channels * 2, channels)
        self.up1_3_mid = unet_plus_comp.Up_Plus(channels * 3, channels * 2, channels)
        self.outconv1 = unet_components.OutConv(channels, out_channels, acticvate, cropping=0)
        self.auxiliary = auxiliary
        if auxiliary:
            self.outconv2 = unet_components.OutConv(channels, cropping=16)
            self.outconv3 = unet_components.OutConv(channels * 2, cropping=16)

    def forward(self, x):
        x0_0 = self.inconv1(x)

        x1_0 = self.down2(x0_0)
        x0_1 = self.up1_1_mid(x0_0, x1_0)

        x2_0 = self.down3(x1_0)
        x1_1 = self.up2_1_mid(x1_0, x2_0)
        x0_2 = self.up1_2_mid(torch.cat((x0_0, x0_1), dim=1), x1_1)


        x3_0 = self.down4(x2_0)
        x2_1 = self.up3_1_mid(x2_0, x3_0)
        x1_2 = self.up2_2_mid(torch.cat((x1_0, x1_1), dim=1), x2_1)
        x0_3 = self.up1_3_mid(torch.cat((x0_0, x0_1, x0_2), dim=1), x1_2)

        x4_0 = self.down5(x3_0)
        x3_1 = self.up4(x3_0, x4_0)
        x2_2 = self.up3(torch.cat((x2_0, x2_1), dim=1), x3_1)
        x1_3 = self.up2(torch.cat((x1_0, x1_1, x1_2), dim=1), x2_2)
        x0_4 = self.up1(torch.cat((x0_0, x0_1, x0_2, x0_3), dim=1), x1_3)

        output1 = self.outconv1(x0_4)

        # for auxiliary classifier
        if self.auxiliary:
            output2 = F.upsample(x1_3, scale_factor=2)
            output2 = self.outconv2(output2)
            output3 = F.upsample(x2_2, scale_factor=4)
            output3 = self.outconv3(output3)
            output = [output1, output2, output3]
        else:
            output = [output1]

        return output
