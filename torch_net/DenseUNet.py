import torch
import torch.nn as nn


class DenseUNet(nn.Module):
    def __init__(self, in_channel, n_classes):
        self.in_channel = in_channel
        self.n_classes = n_classes
        super(DenseUNet, self).__init__()

        '''input 1 -> 32'''
        self.conv0 = self.conv_bn_relu(self.in_channel, 32).cuda(0)

        '''32->64'''
        self.conv1 = self.conv_bn_relu(32, 32 + 8).cuda(0)
        self.conv2 = self.conv_bn_relu(32 + 32 + 8, 64).cuda(0)

        '''64->64'''
        self.pool1 = nn.MaxPool3d(2).cuda(0)
        self.conv3 = self.conv_bn_relu(64, 64).cuda(0)

        '''64->128'''
        self.conv4 = self.conv_bn_relu(64, 64 + 16).cuda(0)
        self.conv5 = self.conv_bn_relu(64 + 64 + 16, 128).cuda(0)

        '''128->128'''
        self.pool2 = nn.MaxPool3d(2).cuda(0)
        self.conv6 = self.conv_bn_relu(128, 128).cuda(0)

        '''128->256'''
        self.conv7 = self.conv_bn_relu(128, 128 + 32).cuda(0)
        self.conv8 = self.conv_bn_relu(128 + 128 + 32, 256).cuda(0)

        '''256->256'''
        self.pool3 = nn.MaxPool3d(2).cuda(0)
        self.conv9 = self.conv_bn_relu(256, 256).cuda(0)

        '''256->512'''
        self.conv10 = self.conv_bn_relu(256, 256 + 64).cuda(0)
        self.conv11 = self.conv_bn_relu(256 + 256 + 64, 512).cuda(0)

        '''512->512'''
        self.deconv1 = self.decoder(512, 512, kernel_size=2, stride=2).cuda(0)
        self.deconv2 = self.decoder(256 + 512, 256, kernel_size=3, stride=1, padding=1).cuda(0)
        self.deconv3 = self.decoder(256, 256, kernel_size=3, stride=1, padding=1).cuda(0)

        self.deconv4 = self.decoder(256, 256, kernel_size=2, stride=2).cuda(0)
        self.deconv5 = self.decoder(128 + 256, 133, kernel_size=3, stride=1, padding=1).cuda(0)
        self.deconv6 = self.decoder(133, 133, kernel_size=3, stride=1, padding=1).cuda(0)

        self.deconv7 = self.decoder(133, 133, kernel_size=2, stride=2).cuda(1)
        self.deconv8 = self.decoder(64 + 133, 133, kernel_size=3, stride=1, padding=1).cuda(1)
        self.deconv9 = self.decoder(133, 133, kernel_size=3, stride=1, padding=1).cuda(2)

    def conv_bn_relu(self, in_channels, out_channels, kernel_size=3, stride=1, padding=1):
        layer = nn.Sequential(
            nn.Conv3d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
            nn.InstanceNorm3d(out_channels),
            nn.ReLU())
        return layer

    def decoder(self, in_channels, out_channels, kernel_size=2, stride=2, padding=0,
                output_padding=0):
        layer = nn.Sequential(
            nn.ConvTranspose3d(in_channels, out_channels, kernel_size, stride=stride,
                               padding=padding, output_padding=output_padding),
            nn.ReLU())
        return layer

    def forward(self, x):
        x = self.conv0(x)
        x1 = self.conv2(torch.cat((self.conv1(x), x), 1))
        del x

        x2 = self.conv3(self.pool1(x1))
        x2 = self.conv5(torch.cat((self.conv4(x2), x2), 1))

        x3 = self.conv6(self.pool2(x2))
        x3 = self.conv8(torch.cat((self.conv7(x3), x3), 1))

        x4 = self.conv9(self.pool3(x3))
        x4 = self.conv11(torch.cat((self.conv10(x4), x4), 1))

        d1 = self.deconv1(x4)
        del x4
        d2 = self.deconv2(torch.cat((d1, x3), 1))
        del x3, d1
        d3 = self.deconv3(d2)
        del d2

        d4 = self.deconv4(d3)
        del d3
        d5 = self.deconv5(torch.cat((d4, x2), 1))
        del x2, d4
        d6 = self.deconv6(d5)
        del d5

        d7 = self.deconv7(d6.cuda(1))
        del d6
        d8 = self.deconv8(torch.cat((d7, x1.cuda(1)), 1))
        del x1, d7
        d9 = self.deconv9(d8.cuda(2))
        del d8
        return d9


def print_network(net):
    num_params = 0
    for param in net.parameters():
        num_params += param.numel()
    print(net)
    print('Total number of parameters: %d' % num_params)


if __name__ == '__main__':
    model = DenseUNet(in_channel=1, n_classes=133)
    print_network(model)





