import torch.nn as nn
from torch_net.unet_components import convs


class InConv(nn.Module):
    def __init__(self, out_channels):
        super().__init__()
        self.model = convs((1, out_channels))

    def forward(self, x):
        x = self.model(x)
        return x


def conv3d(in_channels, out_channels):
    return nn.Conv3d(in_channels, out_channels, kernel_size=3, stride=1, padding=1)


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, in_channels, out_channels, stride=1):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3d(in_channels, out_channels)
        self.bn1 = nn.BatchNorm3d(out_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3d(out_channels, out_channels)
        self.bn2 = nn.BatchNorm3d(out_channels)
        self.stride = stride
        if in_channels != out_channels:
            self.downsample = True
            self.res_conv = nn.Sequential(nn.Conv3d(in_channels, out_channels, kernel_size=1,
                                                    bias=False),
                                          nn.BatchNorm3d(out_channels))
        else:
            self.downsample = False

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample:
            residual = self.res_conv(residual)

        out += residual
        out = self.relu(out)

        return out


class Down(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.conv = BasicBlock(in_channels, out_channels)
        self.pool = nn.MaxPool3d(kernel_size=2, stride=2)

    def forward(self, x):
        x = self.conv(x)
        x = self.pool(x)
        return x
