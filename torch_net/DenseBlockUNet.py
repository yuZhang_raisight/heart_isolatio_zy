import torch
import torch.nn as nn


class DenseBlockUNet(nn.Module):
    def __init__(self, in_channel, n_classes):
        self.in_channel = in_channel
        self.n_classes = n_classes
        super(DenseBlockUNet, self).__init__()

        '''input 1 -> 32'''
        self.conv0 = self.conv_bn_relu(self.in_channel, 32)

        '''dense block1 32->64'''
        self.conv1 = self.dense_conv(32, 32, 8)
        self.conv2 = self.dense_conv(32 + 8, 32, 8)
        self.conv3 = self.dense_conv(32 + 2 * 8, 32, 8)
        self.conv4 = self.dense_conv(32 + 3 * 8, 32, 8)

        '''down pooling1'''
        self.pool1 = nn.MaxPool3d(2)

        '''dense block2 64 -> 128'''
        self.conv5 = self.dense_conv(64, 64, 16)
        self.conv6 = self.dense_conv(64 + 16, 64, 16)
        self.conv7 = self.dense_conv(64 + 2 * 16, 64, 16)
        self.conv8 = self.dense_conv(64 + 3 * 16, 64, 16)

        '''down pooling2'''
        self.pool2 = nn.MaxPool3d(2)

        '''dense block3 128 -> 256'''
        self.conv9 = self.dense_conv(128, 128, 32)
        self.conv10 = self.dense_conv(128 + 32, 128, 32)
        self.conv11 = self.dense_conv(128 + 2 * 32, 128, 32)
        self.conv12 = self.dense_conv(128 + 3 * 32, 128, 32)

        '''down pooling3'''
        self.pool3 = nn.MaxPool3d(2)

        '''dense block4 256 -> 512'''
        self.conv13 = self.dense_conv(256, 256, 64)
        self.conv14 = self.dense_conv(256 + 64, 256, 64)
        self.conv15 = self.dense_conv(256 + 2 * 64, 256, 64)
        self.conv16 = self.dense_conv(256 + 3 * 64, 256, 64)

        '''up1'''
        self.up1 = self.decoder(512, 256)
        self.up1mid = self.conv_bn_relu(512, 128, kernel_size=1, stride=1, padding=0)
        '''up1 dense block'''
        self.up1conv1 = self.dense_conv(128, 128, 32)
        self.up1conv2 = self.dense_conv(128 + 32, 128, 32)
        self.up1conv3 = self.dense_conv(128 + 2 * 32, 128, 32)
        self.up1conv4 = self.dense_conv(128 + 3 * 32, 128, 32)

        '''up2'''
        self.up2 = self.decoder(256, 128)
        self.up2mid = self.conv_bn_relu(256, 133, kernel_size=1, stride=1, padding=0)
        '''up2 dense block'''
        self.up2conv1 = self.dense_conv(133, 133, 16)
        self.up2conv2 = self.dense_conv(133 + 16, 133, 16)
        self.up2conv3 = self.dense_conv(133 + 2 * 16, 133, 16)
        self.up2conv4 = self.dense_conv(133 + 3 * 16, 133, 16)

        '''up3'''
        self.up3 = self.decoder(197, 133)
        self.up3mid = self.conv_bn_relu(133, 133, kernel_size=1, stride=1, padding=0)
        '''up3 dense block'''
        self.up3conv1 = self.dense_conv(133, 133, 8)
        self.up3conv2 = self.dense_conv(133 + 8, 133, 8)
        self.up3conv3 = self.dense_conv(133 + 2 * 8, 133, 8)
        self.up3conv4 = self.dense_conv(133 + 3 * 8, 133, 8)

        '''output'''
        self.conv17 = self.conv_bn_relu(165, self.n_classes, kernel_size=1, stride=1, padding=0)

    def dense_block1(self, x):
        x1 = torch.cat((self.conv1(x), x), 1)
        x2 = torch.cat((self.conv2(x1), x1), 1)
        x3 = torch.cat((self.conv3(x2), x2), 1)
        x4 = torch.cat((self.conv4(x3), x3), 1)
        return x4

    def dense_block2(self, x):
        x1 = torch.cat((self.conv5(x), x), 1)
        x2 = torch.cat((self.conv6(x1), x1), 1)
        x3 = torch.cat((self.conv7(x2), x2), 1)
        x4 = torch.cat((self.conv8(x3), x3), 1)
        return x4

    def dense_block3(self, x):
        x1 = torch.cat((self.conv9(x), x), 1)
        x2 = torch.cat((self.conv10(x1), x1), 1)
        x3 = torch.cat((self.conv11(x2), x2), 1)
        x4 = torch.cat((self.conv12(x3), x3), 1)
        return x4

    def dense_block4(self, x):
        x1 = torch.cat((self.conv13(x), x), 1)
        x2 = torch.cat((self.conv14(x1), x1), 1)
        x3 = torch.cat((self.conv15(x2), x2), 1)
        x4 = torch.cat((self.conv16(x3), x3), 1)
        return x4

    def up1dense_block(self, x):
        x1 = torch.cat((self.up1conv1(x), x), 1)
        x2 = torch.cat((self.up1conv2(x1), x1), 1)
        x3 = torch.cat((self.up1conv3(x2), x2), 1)
        x4 = torch.cat((self.up1conv4(x3), x3), 1)
        return x4

    def up2dense_block(self, x):
        x1 = torch.cat((self.up2conv1(x), x), 1)
        x2 = torch.cat((self.up2conv2(x1), x1), 1)
        x3 = torch.cat((self.up2conv3(x2), x2), 1)
        x4 = torch.cat((self.up2conv4(x3), x3), 1)
        return x4

    def up3dense_block(self, x):
        x1 = torch.cat((self.up3conv1(x), x), 1)
        x2 = torch.cat((self.up3conv2(x1), x1), 1)
        x3 = torch.cat((self.up3conv3(x2), x2), 1)
        x4 = torch.cat((self.up3conv4(x3), x3), 1)
        return x4

    def conv_bn_relu(self, in_channels, out_channels, kernel_size=3, stride=1, padding=1):
        layer = nn.Sequential(
            nn.Conv3d(in_channels, out_channels, kernel_size, stride=stride, padding=padding),
            nn.BatchNorm3d(in_channels),
            nn.ReLU())
        return layer

    def dense_conv(self, in_channels, first_channel, k1):
        layer = nn.Sequential(
            nn.Conv3d(in_channels, first_channel, kernel_size=1, stride=1, padding=0),
            nn.BatchNorm3d(in_channels),
            nn.ReLU(),
            nn.Conv3d(in_channels, k1, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm3d(in_channels),
            nn.ReLU())
        return layer

    def decoder(self, in_channels, out_channels, kernel_size=2, stride=2, padding=0,
                output_padding=0):
        layer = nn.Sequential(
            nn.ConvTranspose3d(in_channels, out_channels, kernel_size, stride=stride,
                               padding=padding, output_padding=output_padding),
            nn.BatchNorm3d(out_channels),
            nn.ReLU())
        return layer

    def forward(self, x):
        x = self.conv0(x)
        x1 = self.dense_block1(x)
        del x

        x2 = self.dense_block2(self.pool1(x1))
        x3 = self.dense_block3(self.pool2(x2))
        x4 = self.dense_block4(self.pool3(x3))

        up1 = self.up1dense_block(self.up1mid(torch.cat((self.up1(x4), x3), 1)))
        up2 = self.up2dense_block(self.up2mid(torch.cat((self.up2(up1), x2), 1)))
        up3 = self.up3dense_block(self.up3mid(torch.cat((self.up3(up2), x1), 1)))
        out = self.conv17(up3)
        del x1, x2, x3, up1, up2, up3
        return out


def print_network(net):
    num_params = 0
    for param in net.parameters():
        num_params += param.numel()
    print(net)
    print('Total number of parameters: %d' % num_params)


if __name__ == '__main__':
    model = DenseBlockUNet(in_channel=1, n_classes=133)
    print_network(model)





