import torch
import torch.nn as nn
import torch.nn.functional as F
epsilon = 1e-7
smooth = 1e-4

def mask_bceloss(y_true, y_pred):
    dtype = y_pred.data.type()
    positive_mask = torch.eq(y_true, 1.0).type(dtype)
    negative_mask = torch.eq(y_true, 0.0).type(dtype)

    weight_pos = (torch.log(torch.sum(negative_mask) + 1) + 1) / \
                 (torch.log(torch.sum(negative_mask) + torch.sum(positive_mask) + 1) + 1)
    weight_neg = (torch.log(torch.sum(positive_mask) + 1) + 1) / \
                 (torch.log(torch.sum(negative_mask) + torch.sum(positive_mask) + 1) + 1)

    loss = nn.BCELoss(size_average=False)
    pos_loss = loss(torch.clamp(positive_mask * y_pred, epsilon, 1-epsilon),
                    torch.clamp(positive_mask * y_true, epsilon, 1-epsilon)) * weight_pos
    neg_loss = loss(torch.clamp(negative_mask * y_pred, epsilon, 1-epsilon),
                    torch.clamp(negative_mask * 0, epsilon, 1-epsilon)) * weight_neg

    return torch.sum(pos_loss + neg_loss)


def mask_bceloss_enhance(y_true, y_pred, positive_enhance = 1):
    dtype = y_pred.data.type()
    positive_mask = torch.eq(y_true, 1.0).type(dtype)
    negative_mask = torch.eq(y_true, 0.0).type(dtype)

    weight_pos = (torch.log(torch.sum(negative_mask) + 1) + 1) / \
                 (torch.log(torch.sum(negative_mask) + torch.sum(positive_mask) + 1) + 1)
    weight_neg = (torch.log(torch.sum(positive_mask) + 1) + 1) / \
                 (torch.log(torch.sum(negative_mask) + torch.sum(positive_mask) + 1) + 1)

    if positive_enhance>0:
        weight_pos = weight_pos * positive_enhance
    loss = nn.BCELoss(size_average=False)
    pos_loss = loss(torch.clamp(positive_mask * y_pred, epsilon, 1-epsilon),
                    torch.clamp(positive_mask * y_true, epsilon, 1-epsilon)) * weight_pos
    neg_loss = loss(torch.clamp(negative_mask * y_pred, epsilon, 1-epsilon),
                    torch.clamp(negative_mask * 0, epsilon, 1-epsilon)) * weight_neg

    return torch.sum(pos_loss + neg_loss)


def attention_bceloss(y_true, y_pred):
    dtype = y_pred.data.type()
    general_mask = torch.eq(y_true, 255).type(dtype)
    attention_mask = torch.ne(y_true, 255).type(dtype)

    general_weight = 1.0
    attention_weight = 1.0
    log_ratio = torch.log((torch.sum(general_mask) + 1)/(torch.sum(attention_mask) + 1))
    if log_ratio > 0.0:
        attention_weight += log_ratio

    loss = nn.BCELoss(size_average=False)
    attention_loss = loss(torch.clamp(attention_mask * y_pred, epsilon, 1-epsilon),
                          torch.clamp(attention_mask * y_true, epsilon, 1-epsilon)) * attention_weight
    general_loss = loss(torch.clamp(general_mask * y_pred, epsilon, 1-epsilon),
                        torch.clamp(general_mask * y_true * 0, epsilon, 1-epsilon)) * general_weight

    return torch.sum(attention_loss + general_loss)



def accuracy(y_true, y_pred):
    dtype = y_pred.data.type()
    positive_mask = torch.eq(y_true, 1.0).type(dtype)
    negative_mask = torch.ne(y_true, 1.0).type(dtype)
    measure_mask = torch.ne(y_true, 255.0).type(dtype)

    y_pred_f = torch.ge(y_pred, 0.5).type(dtype)
    correct_ = torch.sum(torch.eq(measure_mask * y_true, y_pred_f).type(dtype))
    all_ = torch.sum(positive_mask) + torch.sum(negative_mask)

    return correct_ / all_

def accuracy_multi(y_true, y_pred, acti='softmax'):
    if acti == 'softmax':
        dtype = y_pred.data.type()
        positive_mask = y_true[:,0,:,:,:].type(dtype)
        negative_mask = y_true[:,1,:,:,:].type(dtype)
        background_mask = y_true[:,2,:,:,:].type(dtype)

        y_pred = torch.argmax(y_pred, dim=1)
        y_pred_p = torch.eq(y_pred, 0).type(dtype)
        y_pred_n = torch.eq(y_pred, 1).type(dtype)
        y_pred_b = torch.eq(y_pred, 2).type(dtype)

        correct_p = torch.sum(positive_mask * y_pred_p)
        correct_n = torch.sum(negative_mask * y_pred_n)
        correct_b = torch.sum(background_mask * y_pred_b)

        all_ = y_pred.numel()
        return (correct_p + correct_n + correct_b) / all_

def dice_multi(y_true, y_pred, acti='softmax'):
    return -dice_multi_loss(y_true, y_pred, acti=acti)

def dice_multi_loss(y_true, y_pred, acti='softmax'):
    if acti == 'softmax':
        intersection = torch.sum(y_true * y_pred, dim=[0,2,3,4])
        v1 = torch.sum(y_true * y_true, dim=[0,2,3,4])
        v2 = torch.sum(y_pred * y_pred, dim=[0,2,3,4])
        dice_all = -(2. * intersection + smooth) / (v1 + v2 + smooth)
        return torch.mean(dice_all)
    elif acti == 'sigmoid':
        pass

def dice_weight(y_true, y_pred, weight=[1,1,1]):
    intersection = torch.sum(y_true * y_pred, dim=[0, 2, 3, 4])
    v1 = torch.sum(y_true * y_true, dim=[0, 2, 3, 4])
    v2 = torch.sum(y_pred * y_pred, dim=[0, 2, 3, 4])
    dice_all = -(2. * intersection + smooth) / (v1 + v2 + smooth)
    return torch.mean(dice_all * (dice_all.new_tensor(weight)))

def dice(y_true, y_pred):
    dtype = y_pred.data.type()
    y_true_f = torch.eq(y_true, 1.0).type(dtype)
    y_pred_f = torch.ge(y_pred, 0.5).type(dtype)
    intersection = torch.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (torch.sum(y_true_f*y_true_f) + torch.sum(y_pred_f*y_pred_f) + smooth)

def dice_loss(y_true, y_pred):
    intersection = torch.sum(y_true * y_pred)
    return -(2. * intersection + smooth) / (torch.sum(y_true*y_true) + torch.sum(y_pred*y_pred) + smooth)

def accuracy_heart(y_true, y_pred):
    dtype = y_pred.data.type()
    positive_mask = torch.eq(y_true, 1.0).type(dtype)
    negative_mask = torch.ne(y_true, 1.0).type(dtype)

    y_pred_f = torch.ge(y_pred, 0.5).type(dtype)
    y_pred_b = torch.le(y_pred, 0.5).type(dtype)
    correct_f = torch.sum(positive_mask * y_pred_f)
    correct_b = torch.sum(negative_mask * y_pred_b)

    all_ = torch.sum(positive_mask) + torch.sum(negative_mask)

    return (correct_f + correct_b) / all_

def bf1_loss(y_pd, y_gt):
    x_size = y_pd.size()[1]
    y_gt_b = F.max_pool3d(1 - y_gt, kernel_size=3, stride=1, padding=1) - (1 - y_gt)
    y_pd_b = F.max_pool3d(1 - y_pd, kernel_size=3, stride=1, padding=1) - (1 - y_pd)
    y_gt_b_ext = F.max_pool3d(y_gt_b, kernel_size=3, stride=1, padding=1)
    y_pd_b_ext = F.max_pool3d(y_pd_b, kernel_size=3, stride=1, padding=1)
    y_gt_b, y_pd_b, y_gt_b_ext, y_pd_b_ext = y_gt_b.contiguous().view(x_size, -1), y_pd_b.contiguous().view(x_size, -1),\
                                             y_gt_b_ext.view(x_size, -1), y_pd_b_ext.view(x_size, -1)

    p_c = (y_pd_b * y_gt_b_ext).sum(1) / (y_pd_b.sum(1) + 1e-16)
    r_c = (y_gt_b * y_pd_b_ext).sum(1) / (y_gt_b.sum(1) + 1e-16)
    bf1_c = 2 * p_c * r_c / (p_c + r_c + 1e-16)
    return - bf1_c.mean()

def distance_dice(y_true, y_pred, y_mask):
    intersection = torch.sum(y_true * y_pred * y_mask)
    return 1-(2. * intersection + smooth) / (torch.sum(y_true * y_true * y_mask) + torch.sum(y_pred * y_pred * y_mask) + smooth)