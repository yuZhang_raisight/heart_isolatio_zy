import torch
import torch.nn as nn
from torch_net.unet_components import convs


class DenseBlock(nn.Module):
    def __init__(self, in_channels, out_channels, mid_channels):
        super(DenseBlock, self).__init__()
        self.conv1 = convs([in_channels, in_channels + mid_channels])
        self.conv2 = convs([in_channels + in_channels + mid_channels, out_channels])

    def forward(self, x):
        x1 = self.conv1(x)
        cat = torch.cat([x1, x], 1)
        out = self.conv2(cat)
        return out


class Down(nn.Module):

    def __init__(self, in_channels, out_channels, mid_channels):
        super().__init__()
        self.conv = DenseBlock(in_channels, out_channels, mid_channels)
        self.pool = nn.MaxPool3d(kernel_size=2, stride=2)

    def forward(self, x):
        x = self.conv(x)
        x = self.pool(x)
        return x
