import os
import collections
from tensorboardX import SummaryWriter


class TrainLogger:
    def __init__(self, logfile):
        self.logfile = open(logfile, 'a')
        rootdir = os.path.dirname(logfile)
        self.writer = SummaryWriter(log_dir=rootdir)
        self.scalar_keeper = None
        self.global_step = 0
        self.summary_freq = 2

    def __del__(self):
        self.logfile.close()
        self.writer.close()

    def log(self, epoch, idx, info):
        self.global_step += 1

        msg = 'epoch {} iteration {} '.format(epoch, idx)
        for k, v in info.items():
            msg += (k + ' {:0.4f} '.format(float(v)))
        print(msg)
        self.logfile.write(msg + '\n')

        if self.scalar_keeper is None:
            self.scalar_keeper = collections.OrderedDict()
            for key in info:
                self.scalar_keeper[key] = info[key]
        else:
            for key in info:
                self.scalar_keeper[key] += info[key]

        if self.global_step > 0 and self.global_step % self.summary_freq == 0:
            for key in self.scalar_keeper:
                mean_scalar = self.scalar_keeper[key] / self.summary_freq
                self.writer.add_scalar(key, mean_scalar, self.global_step)
                self.scalar_keeper[key] = 0
