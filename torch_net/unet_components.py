import torch
import torch.nn as nn
import torch.nn.functional as F
from milib.imageutils import crop_3dtensor


def convs(channels, activation='relu', usebn=True):
    """
    the channels are a list define. e.g, [16, 32, 24] means that there are two layers.
            first layer input 16 channels, output 32 channels
            second layer input 32 channels, output 24 channels
    """
    models = []
    for i in range(len(channels) - 1):
        in_channels = channels[i]
        out_channels = channels[i + 1]
        models.append(nn.Conv3d(in_channels, out_channels, kernel_size=3, stride=1, padding=1))
        if usebn:
            models.append(nn.BatchNorm3d(num_features=out_channels, eps=1e-3, momentum=0.05))
        if activation == 'relu':
            models.append(nn.ReLU(inplace=True))
        else:
            models.append(nn.Sigmoid())

    return nn.Sequential(*models)

class InConv(nn.Module):
    def __init__(self, out_channels):
        super().__init__()
        self.model = convs((1, out_channels, out_channels))

    def forward(self, x):
        x = self.model(x)
        return x

class InConv_2(nn.Module):  # add 20181108
    def __init__(self, out_channels):
        super().__init__()
        self.model = convs((2, out_channels, out_channels))

    def forward(self, x):
        x = self.model(x)
        return x

class OutConv(nn.Module):

    def __init__(self, in_channels, out_channels=1, acticvate='softmax', cropping=0):
        super().__init__()
        if isinstance(cropping, int):
            cropping = (cropping, cropping, cropping)

        self.cropping = cropping
        if acticvate == 'softmax':
            self.model = nn.Sequential(nn.Conv3d(in_channels, out_channels=out_channels, kernel_size=1),
                                       nn.Softmax(dim=1))
        else:
            self.model = nn.Sequential(nn.Conv3d(in_channels, out_channels=out_channels, kernel_size=1),
                                       nn.Sigmoid())

    def forward(self, x):
        if self.cropping != 0:
            x = crop_3dtensor(x, self.cropping, data_format='channels_first')
        x = self.model(x)
        return x

class Down(nn.Module):

    def __init__(self, in_channels, out_channels, layers=2):
        super().__init__()
        channels = (in_channels,)
        for layer in range(layers):
            channels = channels + (out_channels,)
        self.conv = convs(channels)
        self.pool = nn.MaxPool3d(kernel_size=2, stride=2)

    def forward(self, x):
        x = self.pool(x)
        x = self.conv(x)
        return x

class Down_same(nn.Module):

    def __init__(self, in_channels, out_channels, layers=2):
        super().__init__()
        channels = (in_channels,)
        for layer in range(layers):
            channels = channels + (out_channels,)
        self.conv = convs(channels)
        # self.pool = nn.MaxPool3d(kernel_size=2, stride=2)

    def forward(self, x):
        # x = self.pool(x)
        x = self.conv(x)
        return x

class Up(nn.Module):

    def __init__(self, left_channels, down_channels, out_channels, merge='sum', layers=2):
        """
        Two types of merge: 'sum' or 'cat'
        Notice that the channels are calculated differently for the two types
        """
        super().__init__()

        self.merge = merge
        if merge == 'sum' and left_channels != down_channels:
            raise Exception('the number of channels for left and down input should be equal')
        if merge == 'sum' and left_channels == down_channels:
            in_channels = left_channels
        else:  # for cancatenate
            in_channels = left_channels + down_channels

        channels = (in_channels,)
        for layer in range(layers):
            channels = channels + (out_channels,)
        self.conv = convs(channels)

    def forward(self, x_left, x_down):
        """
        :param x_left: the input from left side
        :param x_down: the input from bottom layer
        :return: output
        """

        # upsample the down input
        x_up = F.interpolate(x_down, scale_factor=2)
        # crop the left input
        size_left = x_left.size()[2:]
        size_up = x_up.size()[2:]
        cropping = [int((size_left[i] - size_up[i])/2) for i in range(3)]
        x_crop = crop_3dtensor(x_left, cropping, data_format='channels_first')

        if self.merge == 'sum' and x_crop.size()[1] != x_up.size()[1]:
            raise Exception('the number of channels for left and down input should be equal')
        elif self.merge == 'sum' and x_crop.size()[1] == x_up.size()[1]:
            merged = x_crop + x_up
        else:
            merged = torch.cat((x_crop, x_up), dim=1)

        x = self.conv(merged)
        return x

class Down_concate(nn.Module):

    def __init__(self, left_channels, down_channels, out_channels, merge='sum', layers=2):
        """
        Two types of merge: 'sum' or 'cat'
        Notice that the channels are calculated differently for the two types
        """
        super().__init__()

        self.merge = merge
        if merge == 'sum' and left_channels != down_channels:
            raise Exception('the number of channels for left and down input should be equal')
        if merge == 'sum' and left_channels == down_channels:
            in_channels = left_channels
        else:  # for cancatenate
            in_channels = left_channels + down_channels

        channels = (in_channels,)
        for layer in range(layers):
            channels = channels + (out_channels,)
        self.conv = convs(channels)
        self.pool = nn.MaxPool3d(kernel_size=2, stride=2)

    def forward(self, x_left, x_up):
        """
        :param x_left: the input from left side
        :param x_down: the input from bottom layer
        :return: output
        """

        # upsample the down input
        if x_left.shape != x_up.shape:
            x_down = F.interpolate(x_left, scale_factor=2)
        else:
            x_down = x_left
        # crop the left input
        size_up = x_up.size()[2:]
        size_down = x_down.size()[2:]
        cropping = [int((size_up[i] - size_down[i])/2) for i in range(3)]
        x_crop = crop_3dtensor(x_up, cropping, data_format='channels_first')

        if self.merge == 'sum' and x_crop.size()[1] != x_down.size()[1]:
            raise Exception('the number of channels for left and down input should be equal')
        elif self.merge == 'sum' and x_crop.size()[1] == x_down.size()[1]:
            merged = x_crop + x_down
        else:
            merged = torch.cat((x_crop, x_down), dim=1)

        x = self.conv(merged)
        x = self.pool(x)

        # x = self.pool(merged)
        # x = self.conv(x)
        return x

class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, in_channels, out_channels,layers=2, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        # self.conv1 = nn.Conv3d(in_channels, out_channels, kernel_size=1, bias=False)
        # self.bn1 = nn.BatchNorm3d(out_channels)
        # self.conv2 = nn.Conv3d(out_channels, out_channels, kernel_size=3, stride=stride,
        #                        padding=1, bias=False)
        # self.bn2 = nn.BatchNorm3d(out_channels)
        # self.conv3 = nn.Conv3d(out_channels, out_channels * self.expansion, kernel_size=1, bias=False)
        # self.bn3 = nn.BatchNorm3d(out_channels * self.expansion)
        # self.relu = nn.ReLU(inplace=True)
        # self.downsample = downsample
        # self.stride = stride

        channels = (in_channels,)
        for layer in range(layers):
            channels = channels + (out_channels,)
        self.conv = convs(channels)
        self.pool = nn.MaxPool3d(kernel_size=2, stride=2)

    def forward(self, x):
        residual = x

        # out = self.conv1(x)
        # out = self.bn1(out)
        # out = self.relu(out)
        #
        # out = self.conv2(out)
        # out = self.bn2(out)
        # out = self.relu(out)
        #
        # out = self.conv3(out)
        # out = self.bn3(out)

        out = self.pool(x)
        out = self.conv(out)

        out += residual
        out = self.relu(out)

        return out
