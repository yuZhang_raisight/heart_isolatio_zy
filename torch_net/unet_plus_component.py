import torch.nn as nn
from torch_net.unet_components import convs
import torch.nn.functional as F
from milib.imageutils import crop_3dtensor

class Up_Plus(nn.Module):

    def __init__(self, left_channels, down_channels, out_channels, merge='sum', layers=2):
        """
        Two types of merge: 'sum' or 'cat'
        Notice that the channels are calculated differently for the two types
        """
        super().__init__()
        self.conv_channel = convs((down_channels, left_channels)) #2020020

        in_channels = left_channels
        channels = (in_channels,)
        for layer in range(layers):
            channels = channels + (out_channels,)
        self.conv = convs(channels)

    def forward(self, x_left, x_down):
        """
        :param x_left: the input from left side
        :param x_down: the input from bottom layer
        :return: output
        """
        x_down = self.conv_channel(x_down)

        # upsample the down input
        x_up = F.upsample(x_down, scale_factor=2)
        # crop the left input
        size_left = x_left.size()[2:]
        size_up = x_up.size()[2:]
        cropping = [int((size_left[i] - size_up[i])/2) for i in range(3)]
        x_crop = crop_3dtensor(x_left, cropping, data_format='channels_first')

        merged = x_crop + x_up
        x = self.conv(merged)
        return x