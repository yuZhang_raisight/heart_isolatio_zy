import torch.nn as nn
import torch.nn.functional as F
import torch_net.unet_components as unet_components

class Unet(nn.Module):

    def __init__(self, channels, out_channels=1, activate='softmax', auxiliary=False):
        super().__init__()
        self.channels = channels
        self.inconv1 = unet_components.InConv(channels)
        self.down2 = unet_components.Down(channels, channels * 2, layers=1)
        self.down3 = unet_components.Down(channels * 2, channels * 4, layers=3)
        self.down4 = unet_components.Down(channels * 4, channels * 8, layers=2)
        self.down5 = unet_components.Down(channels * 8, channels * 8, layers=1)

        # up4 is at the same level as down4
        self.up4 = unet_components.Up(channels * 8, channels * 8, channels * 4)
        # up3 is at the same level as down3
        self.up3 = unet_components.Up(channels * 4, channels * 4, channels * 2)
        # up2 is at the same level as up1
        self.up2 = unet_components.Up(channels * 2, channels * 2, channels)
        # up1 is at the same level as conv1
        self.up1 = unet_components.Up(channels, channels, channels, layers=1)
        self.outconv1 = unet_components.OutConv(channels, out_channels, activate, cropping=0)
        self.auxiliary = auxiliary
        if auxiliary:
            self.outconv2 = unet_components.OutConv(channels, cropping=16)
            self.outconv3 = unet_components.OutConv(channels * 2, cropping=16)

    def forward(self, x):
        x1 = self.inconv1(x)
        x2 = self.down2(x1)
        x3 = self.down3(x2)

        x4 = self.down4(x3)
        x5 = self.down5(x4)
        y4 = self.up4(x4, x5)
        y3 = self.up3(x3, y4)
        y2 = self.up2(x2, y3)
        y1 = self.up1(x1, y2)
        output1 = self.outconv1(y1)

        # for auxiliary classifier
        if self.auxiliary:
            output2 = F.upsample(y2, scale_factor=2)
            output2 = self.outconv2(output2)
            output3 = F.upsample(y3, scale_factor=4)
            output3 = self.outconv3(output3)
            output = [output1, output2, output3]
        else:
            output = [output1]

        return output

def print_network(net):
    num_params = 0
    for param in net.parameters():
        num_params += param.numel()
    print(net)
    print('Total number of parameters: %.1f MB' % (num_params/1024/1024))

if __name__ == '__main__':
    model = Unet(32, 1)
    print_network(model)
