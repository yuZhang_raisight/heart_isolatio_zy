import torch
import torch.nn as nn


class DenseVoxNet(nn.Module):
    def __init__(self, in_channel, out_classes, growrate):
        self.in_channel = in_channel
        self.out_classes = out_classes
        self.growrate = growrate
        super(DenseVoxNet, self).__init__()
        '''input'''
        self.conv0 = self.conv_input(self.in_channel, 32)

        '''dense_block1 32 -> 128'''
        self.conv1 = self.bn_relu_conv(32, self.growrate)
        self.conv2 = self.bn_relu_conv(32 + self.growrate, self.growrate)
        self.conv3 = self.bn_relu_conv(32 + 2 * self.growrate, self.growrate)
        self.conv4 = self.bn_relu_conv(32 + 3 * self.growrate, self.growrate)
        self.conv5 = self.bn_relu_conv(32 + 4 * self.growrate, self.growrate)
        self.conv6 = self.bn_relu_conv(32 + 5 * self.growrate, self.growrate)

        '''down block'''
        self.down1 = self.down_block1(32 + 6 * self.growrate, 448)
        self.down2 = self.down_block2()

        '''dense block2 '''
        self.conv7 = self.bn_relu_conv(448, self.growrate)
        self.conv8 = self.bn_relu_conv(448 + self.growrate, self.growrate)
        self.conv9 = self.bn_relu_conv(448 + 2 * self.growrate, self.growrate)
        self.conv10 = self.bn_relu_conv(448 + 3 * self.growrate, self.growrate)
        self.conv11 = self.bn_relu_conv(448 + 4 * self.growrate, self.growrate)
        self.conv12 = self.bn_relu_conv(448 + 5 * self.growrate, self.growrate)

        '''up block1 main output'''
        self.up1 = self.up_block1(640, self.out_classes)

        '''up block2 side output'''
        self.up2 = self.up_block2(448, self.out_classes)

    def conv_input(self, in_channels, out_channels, kernel_size=3, stride=2, padding=1):
        layer = nn.Sequential(
            nn.Conv3d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=padding))
        return layer

    def dense_block1(self, x):
        x1 = torch.cat((self.conv1(x), x), 1)
        x2 = torch.cat((self.conv2(x1), x1), 1)
        x3 = torch.cat((self.conv3(x2), x2), 1)
        x4 = torch.cat((self.conv4(x3), x3), 1)
        x5 = torch.cat((self.conv5(x4), x4), 1)
        x6 = torch.cat((self.conv6(x5), x5), 1)
        return x6

    def dense_block2(self, x):
        x1 = torch.cat((self.conv7(x), x), 1)
        x2 = torch.cat((self.conv8(x1), x1), 1)
        x3 = torch.cat((self.conv9(x2), x2), 1)
        x4 = torch.cat((self.conv10(x3), x3), 1)
        x5 = torch.cat((self.conv11(x4), x4), 1)
        x6 = torch.cat((self.conv12(x5), x5), 1)
        return x6

    def bn_relu_conv(self, in_channels, out_channels, kernel_size=3, stride=1, padding=1):
        layer = nn.Sequential(nn.BatchNorm3d(in_channels),
                              nn.ReLU(),
                              nn.Conv3d(in_channels, out_channels, kernel_size, stride=stride, padding=padding),
                              nn.Dropout3d(0.2))
        return layer

    def down_block1(self, in_channels, out_channels):
        layer = nn.Sequential(nn.BatchNorm3d(in_channels),
                              nn.ReLU(),
                              nn.Conv3d(in_channels, out_channels, kernel_size=1, stride=1, padding=0))
        return layer

    def down_block2(self):
        layer = nn.Sequential(
            nn.Dropout3d(0.2),
            nn.MaxPool3d(2))
        return layer

    def up_block1(self, in_channels, out_channels):
        layer = nn.Sequential(nn.BatchNorm3d(in_channels),
                              nn.ReLU(),
                              nn.Conv3d(in_channels, in_channels - 160, kernel_size=1, stride=1, padding=0),
                              nn.ConvTranspose3d(in_channels - 160, in_channels - 160 * 2, kernel_size=2, stride=2,
                                                 padding=0, output_padding=0),
                              nn.ReLU(),
                              nn.ConvTranspose3d(in_channels - 160 * 2, in_channels - 160 * 3, kernel_size=2, stride=2,
                                                 padding=0, output_padding=0),
                              nn.ReLU(),
                              nn.Conv3d(in_channels - 160 * 3, out_channels, kernel_size=1, stride=1, padding=0))
        return layer

    def up_block2(self, in_channels, out_channels):
        layer = nn.Sequential(nn.Conv3d(in_channels, int(in_channels / 2), kernel_size=1, stride=1, padding=0),
                              nn.ReLU(),
                              nn.ConvTranspose3d(int(in_channels / 2), out_channels, kernel_size=2, stride=2, padding=0,
                                                 output_padding=0))
        return layer

    def forward(self, x):
        x = self.conv0(x)
        x = self.dense_block1(x)
        x = self.down1(x)
        side = self.up2(x)

        x = self.down2(x)
        x = self.dense_block2(x)
        x = self.up1(x)
        return x, side


def print_network(net):
    num_params = 0
    for param in net.parameters():
        num_params += param.numel()
    print(net)
    print('Total number of parameters: %d' % num_params)


if __name__ == '__main__':
    model = DenseVoxNet(in_channel=1, out_classes=133, growrate=32)
    print_network(model)


