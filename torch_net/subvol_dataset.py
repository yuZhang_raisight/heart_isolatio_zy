import os
import os.path as path
import glob
import random
import numpy as np
import time
import math
import torch
import SimpleITK as sitk
from torch.utils.data import Dataset, DataLoader
from milib import imagehdf, imageutils

class SubvolDataset(Dataset):
    def __init__(self, dataroot, subjectsfile=None, subvol_size=128, spacing=0.1, datalen=None,
                 rotation_range=6.0 * math.pi / 180, rotate=False, noise=False, std=0, norm=False,
                 window=(-1024, 1500), scale=False, translate=False, num_labels=1, dilated=10, enhance=0,
                 cordia=3):

        if subjectsfile:
            with open(subjectsfile) as f:
                subjects = [file.strip() for file in f.readlines()]
        else:
            imgs = glob.glob(os.path.join(dataroot, '*_image.hdf5'))
            subjects = [os.path.basename(img).replace('_image.hdf5', '') for img in imgs]

        random.shuffle(subjects)
        self.subjects = subjects
        self.dataroot = dataroot
        self.rotate = rotate
        self.scale = scale
        self.translate = translate

        self.noise = noise  #hxs add 20181022
        self.std = std #hxs add 20181027
        self.norm = norm
        self.window = window
        self.num_labels = num_labels
        self.dilated = dilated
        self.enhance = enhance
        self.cordia = cordia

        def parse_subsize(subvol_size):
            if isinstance(subvol_size, int):
                result = (subvol_size, subvol_size, subvol_size)
            elif len(subvol_size) == 3:
                result = subvol_size
            else:
                raise Exception('wrong subvolume size')
            return result

        self.subvol_size = parse_subsize(subvol_size)
        self.spacing = (spacing, spacing, spacing)
        self.rotation_range = rotation_range
        if datalen is None:
            self.len = len(subjects)
        else:
            self.len = datalen

    def __len__(self):
        return self.len

    def __getitem__(self, idx):
        subject = self.subjects[idx % len(self.subjects)]
        sample = self._generate_sample(subject, noise=self.noise, std=self.std)
        return sample

    def _generate_sample(self, subject, noise=False, std=0):
        imagefile = path.join(self.dataroot, subject + '_image.hdf5')
        labelfile = path.join(self.dataroot, subject + '_label.hdf5')
        maskfile = path.join(self.dataroot, subject + '_coronary.hdf5')
        center = imagehdf.get_center_h5image(imagefile)

        if self.rotate:
            if random.random() > 0.6:
                rot_angle = np.random.rand(3) * 2 * self.rotation_range - self.rotation_range
        else:
            rot_angle = (0, 0, 0)

        if self.window[0] != self.window[1]:
            default_value = self.window[0]
        else:
            default_value = imagehdf.get_min_value_h5image(imagefile)

        sampled_image = imagehdf.resample_h5image(imagefile, center=center, size=self.subvol_size,
                                                  spacing=self.spacing, rot_angles=rot_angle, default_value=default_value,
                                                  interp='linear')
        sitk.WriteImage(sampled_image, '/home/yuzhang/virtual shared/heart_mask_patent/img.nii')
        sampled_label = imagehdf.resample_h5image(labelfile, center=center, size=self.subvol_size, spacing=self.spacing,
                                                  rot_angles=rot_angle, default_value=0, interp='nearest')
        if self.window[0] != self.window[1]:
            sampled_image = sitk.IntensityWindowing(sampled_image, self.window[0], self.window[1],
                                                    self.window[0], self.window[1])
        if self.scale and random.random() > 0.6:
            scale = random.uniform(0.9, 1.1)
            sampled_image = imageutils.get_augmented_sitkimage(sampled_image, scale, (0,0,0), sitk.sitkLinear, default_value)
            sampled_label = imageutils.get_augmented_sitkimage(sampled_label, scale, (0,0,0), sitk.sitkNearestNeighbor, 0)
        else:
            scale = 1

        if self.translate and random.random() > 0.6:
            translate = np.random.rand(3) * 2 * 2 - 2 # -2cm ~ 2cm
            sampled_image = imageutils.get_augmented_sitkimage(sampled_image, 1, translate, sitk.sitkLinear,
                                                               default_value)
            sampled_label = imageutils.get_augmented_sitkimage(sampled_label, 1, translate,
                                                                   sitk.sitkNearestNeighbor, 0)
        else:
            translate = (0, 0, 0)

        if self.norm:
            sampled_image = sitk.RescaleIntensity(sampled_image, 0, 1)
        if noise:
            if random.random()>0.2:
                sampled_image_np = sitk.GetArrayFromImage(sampled_image)

                sampled_noise_label = imagehdf.resample_h5image(labelfile, center=center, size=self.subvol_size,
                                                                spacing=self.spacing, rot_angles=rot_angle,
                                                                default_value=255, interp='nearest')
                sampled_noise_label_np = sitk.GetArrayFromImage(sampled_noise_label)
                sampled_noise_label_np = np.where(1 == sampled_noise_label_np, 1, 0)
                noise_matrix = std * np.random.randn(sampled_image_np.shape[0], sampled_image_np.shape[1], sampled_image_np.shape[2])
                result = sampled_noise_label_np * noise_matrix

                sampled_image_np = sampled_image_np + result
                sampled_image = sitk.GetImageFromArray(sampled_image_np)

        if self.num_labels == 3:
            sampled_label = sitk.Cast(sampled_label, sitk.sitkUInt8)
            dilated_label = sitk.BinaryDilate(sampled_label, self.dilated)
            negative_label = sitk.Subtract(dilated_label, sampled_label)
            background_label = sitk.Not(dilated_label)

        def convert_to_tensor(img, dtype=torch.float):
            if isinstance(img, sitk.SimpleITK.Image):
                img = sitk.GetArrayFromImage(img)
            img = np.reshape(img, (1,) + img.shape)
            tensor = torch.from_numpy(img).type(dtype)
            return tensor

        x = convert_to_tensor(sampled_image)
        if self.num_labels == 3:
            y1 = convert_to_tensor(sampled_label)
            y2 = convert_to_tensor(negative_label)
            y3 = convert_to_tensor(background_label)
            y = torch.cat((y1,y2,y3), dim=0)
        else:
            y = convert_to_tensor(sampled_label)

        if self.enhance != 0:
            sampled_mask = imagehdf.resample_h5image(maskfile, center=center, size=self.subvol_size,
                                                      spacing=self.spacing,
                                                      rot_angles=rot_angle, default_value=0, interp='nearest')
            sampled_mask = imageutils.get_augmented_sitkimage(sampled_mask, scale, translate,
                                                                   sitk.sitkNearestNeighbor, 0)
            sampled_mask = sitk.Cast(sampled_mask, sitk.sitkUInt8)
            sampled_mask = sitk.BinaryDilate(sampled_mask, self.cordia)
            sampled_mask = sampled_mask * self.enhance
            z = convert_to_tensor(sampled_mask)

            sampled_label = sitk.Cast(sampled_label, sitk.sitkUInt8)
            sampled_mask = sitk.DanielssonDistanceMap(sampled_label, True, useImageSpacing=True)
            arr = sitk.GetArrayFromImage(sampled_mask)
            arr[arr < 0] = 255
            arr[arr > 1] = 255
            arr[arr == 255] = 0
            arr = 1 - arr
            arr = arr * 9 + 1
            arr[arr == 10] = 1
            z1 = convert_to_tensor(arr)
            return {'image': x, 'label': y, 'mask': z, 'mask1': z1}
        else:
            sampled_label = sitk.Cast(sampled_label, sitk.sitkUInt8)
            sampled_mask = sitk.DanielssonDistanceMap(sampled_label, True, useImageSpacing=True)
            arr = sitk.GetArrayFromImage(sampled_mask)
            arr[arr < 0] = 255
            arr[arr > 1] = 255
            arr[arr == 255] = 0
            arr = 1 - arr
            arr = arr * 9 + 1
            arr[arr == 10] = 1
            z = convert_to_tensor(arr)
            return {'image': x, 'label': y, 'mask': z}

def test_generator_abodominal():
    dataroot = '/media/data/heart_isolation/hdf5'
    subvol_size = (176, 176, 128)
    spacing = 0.15
    dataset = SubvolDataset(dataroot=dataroot, spacing=spacing, subvol_size=subvol_size, datalen=28,norm=True)
    dataloader = DataLoader(dataset, batch_size=1, shuffle=True, num_workers=1)
    t0 = time.time()
    for i, data in enumerate(dataloader):
        print('data shape', data['image'].shape)
        print('data type', type(data['image']))
        t1 = time.time()
        print('tictoc', i, t1 - t0)

        x1 = np.squeeze(data['image'].numpy()[0, 0, :, :, :])
        y1 = np.squeeze(data['label'].numpy()[0, 0, :, :, :])
        x1img = sitk.GetImageFromArray(x1)
        y1img = sitk.GetImageFromArray(y1)
        sitk.WriteImage(x1img, '/media/data/heart_isolation/img.nii')
        sitk.WriteImage(y1img, '/media/data/heart_isolation/lab.nii.gz')

        print('x1 shape', x1.shape)
        print('y1 shape', y1.shape)
        print(0)


if __name__ == '__main__':
    # test_generator()
    test_generator_abodominal()
