import numpy as np
import SimpleITK as sitk
import shutil
import glob
import milib.imageutils as imageutils
import h5py as h5
import os
from train_test_scripts.test import parse_model_name

# img = sitk.ReadImage('/media/data/ROI-2-2/146_081_LWFE/146_081_LWFE.nii')
# lab = sitk.ReadImage('/media/data/ROI-2-2/146_081_LWFE/146_081_LWFE_mask-label.nii.gz')
# out = sitk.Mask(img, lab, outsideValue=-1024)
# sitk.WriteImage(out, '/media/data/ROI-2-2/146_081_LWFE/146_081_LWFE_mask.nii')

# img = sitk.ReadImage('/media/data/ROI-2/ROI-01/125_080_LIGA.nii')
# img_iso = imageutils.resample_image(img, spacing_out=(0.15,0.15,0.15), size_out=(256,256,128), resample_method=sitk.sitkLinear, default_value=-1024)
# sitk.WriteImage(img_iso, '/media/data/ROI-2/ROI-01/125_080_LIGA_test.nii')

# img_dir = '/home/yuzhang/Downloads/heart_CT/ct_train1_88152'
# imglist = [os.path.join(img_dir, f) for f in sorted(os.listdir(img_dir)) if 'image' in f]
# lablist = [os.path.join(img_dir, f) for f in sorted(os.listdir(img_dir)) if 'label' in f]
# for l in lablist:
#     lab = sitk.ReadImage(l)
#     labarr = sitk.GetArrayFromImage(lab)
#     labarr[labarr > 0] = 1
#     out = sitk.GetImageFromArray(labarr)
#     out.CopyInformation(lab)
#     sitk.WriteImage(out, l.replace('_label.nii.gz', '_heart_label.nii.gz'))

# img = sitk.ReadImage('/media/data/ROI-2/ROI-01/125_080_LIGA-label.nii.gz')
# arr = sitk.GetArrayFromImage(img)
# arr = arr.astype(np.float32)
# arr[arr == 1] = 0.6
# arr[arr == 0] = 0.1
# out = sitk.GetImageFromArray(arr)
# sitk.WriteImage(out, '/media/data/ROI-2/ROI-01/test.nii.gz')

# img = sitk.ReadImage('/media/data/ROI-2/ROI-01/test.nii.gz')
# orig_seg = sitk.BinaryThreshold(img, 0.5, 1.0)
# sitk.WriteImage(orig_seg, '/media/data/ROI-2/ROI-01/test1.nii.gz')



# pth = '/media/data/heart_isolation/nii'
# imgs = [os.path.join(pth, f) for f in sorted(os.listdir(pth))]
# import random
# slice = random.sample(imgs, 2)
# print(slice)
#
# for i in imgs:
#     fn = os.path.basename(i)
#     img = sitk.ReadImage(os.path.join(i, fn+'_label.nii.gz'))
#     size= np.array(img.GetSize())
#     spacing = np.array(img.GetSpacing())
#     print(size*spacing/0.2)

# pth = '/media/data/heart_isolation/nii_test'
# imgs = [os.path.join(pth, f) for f in sorted(os.listdir(pth)) if 'image' in f]
# for i in imgs:
#     img = sitk.ReadImage(i)
#     fn = os.path.basename(i)
#     sub = fn.split('_image')[0]
#     mask_pth = '/media/data/heart_isolation/test/train_datalen8_bs1_gm0.98_lr0.001_sz176_176_128_spc0.15_normFalse_rotateFalse_ch32_ep52/' + sub + '_seg0.5.nii.gz'
#     mask = sitk.ReadImage(mask_pth)
#     maskarr = sitk.GetArrayFromImage(mask)
#     imgarr = sitk.GetArrayFromImage(img)
#     imgarr[maskarr == 0] = -1024
#     out = sitk.GetImageFromArray(imgarr)
#     out.CopyInformation(img)
#     sitk.WriteImage(out, os.path.join(pth, sub + '_mask.nii'))

# from train_test_scripts.test import find_best_epoch
# def best_epoch_analysis(modelspth='/media/data/heart_isolation'):
#     models = [os.path.join(modelspth, f) for f in sorted(os.listdir(modelspth)) if 'train_' in f]
#     for i in models:
#         logfile = i + '/log.txt'
#         epoch_l = find_best_epoch(logfile, indicator='loss')
#         epoch_d = find_best_epoch(logfile, indicator='dice')
#         print('loss:'+str(epoch_l)+', dice:'+str(epoch_d))
# best_epoch_analysis()

# def batch_test(modelspth='/media/data/heart_isolation'):
#     models = [os.path.join(modelspth, f) for f in sorted(os.listdir(modelspth)) if 'train_' in f]
#     n = 0
#     pth = '/home/yuzhang/PycharmProjects/bone_removal/train_test_scripts'
#     for i in models:
#         with open(pth+'/batch_test'+str(n)+'.sh', 'w') as f:
#             f.write('python test.py --model_folder ' + i + '--indicator loss')
#             n += 1
#         with open(pth+'/batch_test' + str(n) + '.sh', 'w') as f:
#             f.write('python test.py --model_folder ' + i + '--indicator dice')
#             n += 1
#
# batch_test()

# pth = '/media/data/heart_isolation/test/train_datalen16_bs1_gm0.98_lr0.001_lossBCE_sz176_176_128_spc0.15_normFalse_rotateFalse_ch32_ep42/030_026_YGCH_xzw_seg0.5_testing_statistic.csv'
# with open(pth, 'r') as f:
#     lines = f.readlines()[1]
#     stat = lines.split('\t')
#     sensitivity, specificity, dice, false_positive = stat[1], stat[2], stat[3], stat[4]
#     print(0)

# from milib.imageutils import get_augmented_sitkimage
# import random
# img = sitk.ReadImage('/media/data/heart_isolation/nii/01_PKR_20171113/01_PKR_20171113_image.nii')
# lab = sitk.ReadImage('/media/data/heart_isolation/nii/01_PKR_20171113/01_PKR_20171113_label.nii.gz')
# translate = np.random.rand(3) * 2 * 2 - 2
# print(translate)
# sampled_image = imageutils.get_augmented_sitkimage(img, 1, translate, sitk.sitkLinear, -1024)
# sampled_label = imageutils.get_augmented_sitkimage(lab, 1, translate, sitk.sitkNearestNeighbor, 0)
# sitk.WriteImage(sampled_image, '/media/data/heart_isolation/nii/01_PKR_20171113/01_PKR_20171113_image_sample.nii')
# sitk.WriteImage(sampled_label, '/media/data/heart_isolation/nii/01_PKR_20171113/01_PKR_20171113_label_sample.nii.gz')

def data_analysis_heart_volume(datapth='/media/data/heart_isolation'):
    import glob
    import shutil
    training_pth = datapth + '/nii'
    training_imgs = glob.glob(training_pth+'/*/*_label.nii.gz')
    testing_pth = datapth + '/nii_test'
    testing_imgs = glob.glob(testing_pth+'/*_label.nii.gz')
    imgs = training_imgs + testing_imgs
    outdir = '/media/data/heart_isolation/manual_labels/labels_v1'
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    for i in imgs:
        fn = os.path.basename(i)
        shutil.copy(i, os.path.join(outdir, fn))

# labels_v2 = '/media/data/heart_isolation/manual_labels/dialate_label'
# labels_v1 = '/media/data/heart_isolation/manual_labels/labels_v1'
# imgs_v2 = [f for f in sorted(os.listdir(labels_v2))]
# imgs_v1 = [f for f in sorted(os.listdir(labels_v1))]
# for i, j in zip(imgs_v1, imgs_v2):
#     if i != j:
#         print(i + '\t' + j)

def replace_labels_version():
    import glob
    import shutil
    datapth = '/media/data/heart_isolation'
    training_pth = datapth + '/nii'
    training_imgs = glob.glob(training_pth + '/*/*_label.nii.gz')
    testing_pth = datapth + '/nii_test'
    testing_imgs = glob.glob(testing_pth + '/*_label.nii.gz')
    imgs = training_imgs + testing_imgs
    labelsdir = '/media/data/heart_isolation/manual_labels/dialate_label'
    for i in imgs:
        fn = os.path.basename(i)
        shutil.copy(os.path.join(labelsdir, fn), i)

# from train_test_scripts.test import find_best_epoch
# model_floders = '/media/data/heart_isolation/train_v2'
# models = [os.path.join(model_floders, f) for f in sorted(os.listdir(model_floders))]
# for model_folder in models:
#     logfile = model_folder + '/log.txt'
#     test_data_dir = r'/media/data/heart_isolation/nii_test'
#     epoch = find_best_epoch(logfile, indicator='loss')
#     s = model_folder.split('_loss')[-1]
#     s = s.split('_sz')[0]
#     print(s + ':' + str(epoch))

coronarydirs = '/media/data/heart_isolation/manual_labels/coronary_labels'
coronarys = [os.path.join(coronarydirs, f) for f in sorted(os.listdir(coronarydirs))]

import shutil
def move_coronary2nii():
    nii_dir = '/media/data/heart_isolation/nii'
    niis = sorted(os.listdir(nii_dir))
    for nii in niis:
        cor = os.path.join(coronarydirs, nii+'_label.nii.gz')
        target = os.path.join(nii_dir, nii, nii+'_coronary.nii.gz')
        shutil.copy(cor, target)

# import pydicom
# dcm_dir = '/media/data/For_ZhangYu'
# dcm_series = [os.path.join(dcm_dir, f) for f in sorted(os.listdir(dcm_dir)) if 'nii' not in f]
# for i in dcm_series:
#     reader = sitk.ImageSeriesReader()
#     img_names = reader.GetGDCMSeriesFileNames(i)
#     reader.SetFileNames(img_names)
#     image = reader.Execute()
#     spacing = image.GetSpacing()
#     new_spacing = [s / 10 for s in spacing]
#     image.SetSpacing(new_spacing)
#     sitk.WriteImage(image, i + '_image.nii')

# dcm_dir = '/media/data/cases_collect_for_Jun/cases/XuWang'
# nii_list = [os.path.join(dcm_dir, f) for f in sorted(os.listdir(dcm_dir)) if 'nii' in f]
# for i in nii_list:
#     img = sitk.ReadImage(i)
#     print(img.GetSize())
#     print(img.GetSpacing())

# dcm_dir = '/media/data/heart_isolation/nii'
# nii_list = [os.path.join(dcm_dir, f) for f in sorted(os.listdir(dcm_dir))]
# for i in nii_list:
#     fn = os.path.basename(i)
#     img = sitk.ReadImage(os.path.join(i, fn+'_label.nii.gz'))
#     spacing = img.GetSpacing()
#     size = img.GetSize()
#     out_size = [sz*sp/0.2 for sz, sp in zip(size, spacing)]
#     print(out_size)

# img = sitk.ReadImage('/media/data/heart_isolation/nii/01_PKR_20171113/01_PKR_20171113_arota_label.nii.gz')
# out = sitk.DanielssonDistanceMap(img, True, useImageSpacing=True)
# sitk.WriteImage(out, '/media/data/heart_isolation.nii')

# import numpy as np
# import matplotlib.pyplot as plt
# from scipy import interpolate
# from mpl_toolkits.mplot3d import Axes3D
#
#
# # 3D example
# total_rad = 10
# z_factor = 3
# noise = 0.1
#
# num_true_pts = 200
# s_true = np.linspace(0, total_rad, num_true_pts)
# x_true = np.cos(s_true)
# y_true = np.sin(s_true)
# z_true = s_true/z_factor
#
# num_sample_pts = 80
# s_sample = np.linspace(0, total_rad, num_sample_pts)
# x_sample = np.cos(s_sample) + noise * np.random.randn(num_sample_pts)
# y_sample = np.sin(s_sample) + noise * np.random.randn(num_sample_pts)
# z_sample = s_sample/z_factor + noise * np.random.randn(num_sample_pts)
#
# tck, u = interpolate.splprep([x_sample,y_sample,z_sample], s=2)
# x_knots, y_knots, z_knots = interpolate.splev(tck[0], tck)
# u_fine = np.linspace(0,1,num_true_pts)
# x_fine, y_fine, z_fine = interpolate.splev(u_fine, tck)
#
# fig2 = plt.figure(2)
# ax3d = fig2.add_subplot(111, projection='3d')
# ax3d.plot(x_true, y_true, z_true, 'b')
# ax3d.plot(x_sample, y_sample, z_sample, 'r*')
# ax3d.plot(x_knots, y_knots, z_knots, 'go')
# ax3d.plot(x_fine, y_fine, z_fine, 'g')
# fig2.show()
# plt.show()

# img_dir = '/media/data/CT_quality_analysis1'
# img_list = [os.path.join(img_dir, f) for f in sorted(os.listdir(img_dir))]
# for i in img_list:
#     img = sitk.ReadImage(i)
#     sitk.WriteImage(img, i.replace('.nii.gz', '_image.nii'))
#     os.remove(i)


def sv2niifolder(sv_dir, rst_dir):
    if not os.path.exists(rst_dir):
        os.makedirs(rst_dir)
    sv_list = [os.path.join(sv_dir, f) for f in sorted(os.listdir(sv_dir))]
    for i in sv_list:
        fn = os.path.basename(i)
        img_pth = os.path.join(i, 'Images')
        img_list = glob.glob(os.path.join(img_pth, '*.nii'))
        tag_pth = os.path.join(i, 'TagImages')
        tag_list = glob.glob(os.path.join(tag_pth, '*.nii'))
        nii_pth = img_list[0] if img_list != [] else tag_list[0]
        shutil.copy(nii_pth, os.path.join(rst_dir, fn + '_image.nii'))


def spcing_analysis(img_dir):
    img_list = [os.path.join(img_dir, f) for f in os.listdir(img_dir)]
    for i in img_list:
        img = sitk.ReadImage(i)
        sp = img.GetSpacing()
        if sp[0] > 0.1:
            sp = np.array(sp) / 10
            ogn = np.array(img.GetOrigin()) / 10
            img.SetSpacing(sp)
            img.SetOrigin(ogn)
            sitk.WriteImage(img, i)


def get_dilate(img_dir, dilate_dir):
    if not os.path.exists(dilate_dir):
        os.makedirs(dilate_dir)
    img_list = [os.path.join(img_dir, f) for f in os.listdir(img_dir)]
    for i in img_list:
        img = sitk.ReadImage(i)
        fn = os.path.basename(i)
        for j in range(3):
            img = sitk.BinaryDilate(img, 1)
        sitk.WriteImage(img, os.path.join(dilate_dir, fn))


def get_mask(img_dir, dilate_dir, mask_dir):
    if not os.path.exists(mask_dir):
        os.makedirs(mask_dir)
    img_list = [os.path.join(img_dir, f) for f in sorted(os.listdir(img_dir))]
    dilate_list = [os.path.join(dilate_dir, f) for f in sorted(os.listdir(dilate_dir))]
    for i, j in zip(img_list, dilate_list):
        img = sitk.ReadImage(i)
        lab = sitk.ReadImage(j)
        mask = sitk.Mask(img, lab, -1024)
        fn = os.path.basename(i)
        sitk.WriteImage(mask, os.path.join(mask_dir, fn))


def mask2sv(mask_dir, sv_dir):
    mask_list = [os.path.join(mask_dir, f) for f in sorted(os.listdir(mask_dir))]
    for i in mask_list:
        fn = os.path.basename(i)
        fn = fn.split('_image')[0]
        rst_dir = os.path.join(sv_dir, fn, 'Mask')
        if not os.path.exists(rst_dir):
            os.makedirs(rst_dir)
        shutil.copy(i, os.path.join(rst_dir, fn + '.nii'))


if __name__ == '__main__':
    # 1
    # sv_pth = '/home/yuzhang/Downloads/20_07_10_hospital_review/cases'
    # rst_dir = '/media/data/heart_isolation/test_for_wangxu/ori_images'
    # sv2niifolder(sv_pth, rst_dir)
    # 2
    # img_dir = '/media/data/heart_isolation/test_for_wangxu/ori_images'
    # spcing_analysis(img_dir)
    # 3
    # img_dir = '/media/data/heart_isolation/test_for_wangxu/labels'
    # dilate_dir = '/media/data/heart_isolation/test_for_wangxu/dialte'
    # get_dilate(img_dir, dilate_dir)
    # 4
    # img_dir = '/media/data/heart_isolation/test_for_wangxu/ori_images'
    # dilate_dir = '/media/data/heart_isolation/test_for_wangxu/dialte'
    # mask_dir = '/media/data/heart_isolation/test_for_wangxu/mask'
    # get_mask(img_dir, dilate_dir, mask_dir)
    # 5
    mask_dir = '/media/data/heart_isolation/test_for_wangxu/mask'
    sv_dir = '/home/yuzhang/Downloads/20_07_10_hospital_review/cases'
    mask2sv(mask_dir, sv_dir)

