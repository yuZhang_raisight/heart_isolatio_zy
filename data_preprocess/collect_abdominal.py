import os
import os.path as path
import SimpleITK as sitk
import numpy as np
import glob
import milib.imagehdf as imagehdf
import milib.imageutils as imageutils

def convert_image(imgfile, desroot):
    imgh5 = path.join(desroot, path.basename(imgfile).replace('.nii', '.hdf5'))
    imagehdf.write_h5image(imgfile, imgh5) # 1.'/media/data/ROI-2/ROI-01/125_080_LIGA.hdf5'

def convert_seg(segfile, desroot):
    segh5 = path.join(desroot, path.basename(segfile).replace('.nii.gz', '.hdf5'))
    imagehdf.write_h5image(segfile, segh5)  # 3.'/home/huangxs/data/Kidney_part1/hdf5/ZHAO_YIJUN_artery_label.hdf5'

def batch_convert(srcroot=r'/media/data/heart_isolation/nii', desroot=r'/media/data/heart_isolation/hdf5'):

    if not os.path.exists(desroot):
        os.makedirs(desroot)

    for pat in sorted(os.listdir(srcroot)):
        imgs = glob.glob(os.path.join(srcroot, pat, '*_image.nii'))   #['/home/huangxs/data/Kidney_part1/nii/ZHAO_YIJUN/ZHAO_YIJUN_image.nii']
        if not imgs:
            continue

        imgfile = imgs[0]
        imgh5 = os.path.join(desroot, os.path.basename(imgfile).replace('.nii', '.hdf5'))   #'/home/huangxs/data/Kidney_part1/hdf5/ZHAO_YIJUN_image.hdf5'
        if os.path.exists(imgh5):
            continue

        convert_image(imgfile, desroot)  # process  1.'/home/huangxs/data/Kidney_part1/hdf5/ZHAO_YIJUN_image.hdf5'
        print('process', imgfile)

        segfiles = glob.glob(os.path.join(srcroot, pat, '*_arota_label.nii.gz')) #['/home/huangxs/data/Kidney_part1/nii/ZHAO_YIJUN/ZHAO_YIJUN_artery_label.nii.gz']
        if not segfiles:
            continue
        segfile = segfiles[0]
        print('process', segfile)
        convert_seg(segfile, desroot)

        coronaryfiles = glob.glob(os.path.join(srcroot, pat,
                                          '*_coronary.nii.gz'))  # ['/home/huangxs/data/Kidney_part1/nii/ZHAO_YIJUN/ZHAO_YIJUN_artery_label.nii.gz']
        if not coronaryfiles:
            continue
        coronaryfile = coronaryfiles[0]
        print('process', coronaryfile)
        convert_seg(coronaryfile, desroot)

def images_to_folder(srcroot='/media/data/heart_isolation/manual_labels/case12'):
    import shutil
    imgs = [os.path.join(srcroot, f) for f in sorted(os.listdir(srcroot)) if 'image' in f]
    labs = [os.path.join(srcroot, f) for f in sorted(os.listdir(srcroot)) if 'label' in f]
    for i, j in zip(imgs, labs):
        fni = os.path.basename(i)
        fnj = os.path.basename(j)
        sub = fni.split('_image')[0]
        os.mkdir(os.path.join(srcroot, sub))
        shutil.move(i, os.path.join(srcroot, sub, fni))
        shutil.move(j, os.path.join(srcroot, sub, fnj))

def rename_nii(dir='/media/data/heart_isolation/manual_labels/case12'):
    imgs = [os.path.join(dir, f) for f in sorted(os.listdir(dir)) if 'mask' not in f]
    labs = [os.path.join(dir, f) for f in sorted(os.listdir(dir)) if 'mask' in f]
    for i in imgs:
        os.rename(i, i.replace('.nii', '_image.nii'))

    for i in labs:
        os.rename(i, i.replace('_mask-', '_'))

def data_analysis(datapth='/media/data/heart_isolation'):
    training_pth = datapth + '/nii'
    training_imgs = glob.glob(training_pth+'/*/*_image.nii')
    testing_pth = datapth + '/nii_test'
    testing_imgs = glob.glob(testing_pth+'/*_image.nii')
    imgs = training_imgs + testing_imgs
    for i in imgs:
        fn = os.path.basename(i)
        imgID = fn.split('_image')[0]
        img = sitk.ReadImage(i)
        size = img.GetSize()
        spacing = img.GetSpacing()
        size = [str(f) for f in size]
        spacing = [str(f) for f in spacing]
        imgarr = sitk.GetArrayFromImage(img)
        imax = str(imgarr.max())
        imin = str(imgarr.min())
        print(imgID + '\t' + size[0] + '\t' + size[1] + '\t' + size[2] + '\t' + spacing[0] + '\t' + spacing[1] + '\t' +
              spacing[2] + '\t' + imax + '\t' + imin + '\t')

def data_analysis_heart_volume(datapth='/media/data/heart_isolation'):
    training_pth = datapth + '/nii'
    training_imgs = glob.glob(training_pth+'/*/*_label.nii.gz')
    testing_pth = datapth + '/nii_test'
    testing_imgs = glob.glob(testing_pth+'/*_label.nii.gz')
    imgs = training_imgs + testing_imgs
    for i in imgs:
        fn = os.path.basename(i)
        imgID = fn.split('_label')[0]
        img = sitk.ReadImage(i)
        stat = sitk.LabelShapeStatisticsImageFilter()
        stat.Execute(img)
        heart_volume = stat.GetPhysicalSize(1)
        print(imgID + '\t' + str(heart_volume))

def merge_arota_to_label(img_dir='/media/data/heart_isolation/nii'):
    img_list = [os.path.join(img_dir, f) for f in sorted(os.listdir(img_dir))]
    for i in img_list:
        fn = os.path.basename(i)
        lab = sitk.ReadImage(os.path.join(i, fn+'_label.nii.gz'))
        aro = sitk.ReadImage(os.path.join(i, fn+'_coronary.nii.gz'))
        lab, aro = sitk.Cast(lab, sitk.sitkUInt8), sitk.Cast(aro, sitk.sitkUInt8)

        out = sitk.Or(lab, aro)
        sitk.WriteImage(out, os.path.join(i, fn+'_arota_label.nii.gz'))


if __name__ == '__main__':
    # rename_nii()
    # images_to_folder()
    # convert_image('/media/data/ROI-2/ROI-01/125_080_LIGA.nii', '/media/data/ROI-2/ROI-01')
    # batch_convert()
    # data_analysis()
    # data_analysis_heart_volume()
    batch_convert(srcroot=r'/media/data/heart_isolation/nii',
                  desroot=r'/media/data/heart_isolation/hdf5_v3')
    # merge_arota_to_label()